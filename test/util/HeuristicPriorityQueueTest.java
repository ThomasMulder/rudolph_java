package util;

import automaton.state.DataState;
import automaton.state.State;
import graph.Node;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class HeuristicPriorityQueueTest {

    @Test
    public void fifoTest() {
        DataState d1 = new DataState("d1");
        DataState d2 = new DataState("d2");
        DataState d3 = new DataState("d3");
        Map<State, Integer> heuristic = new HashMap<>();
        heuristic.put(d1, 5);
        heuristic.put(d2, 3);
        heuristic.put(d3, 1);
        Run r1 = new Run(Node.EMPTY_NODE, d1, null, 0, 0);
        Run r2 = new Run(Node.EMPTY_NODE, d2, null, 0, 2);
        Run r3 = new Run(Node.EMPTY_NODE, d3, null, 0, 4);

        HeuristicPriorityQueue instance = new HeuristicPriorityQueue(heuristic);
        instance.insert(r1);
        instance.insert(r2);
        instance.insert(r3);
        assertEquals(r1, instance.poll());
        assertEquals(r2, instance.poll());
        assertEquals(r3, instance.poll());

        instance = new HeuristicPriorityQueue(heuristic);
        instance.insert(r3);
        instance.insert(r2);
        instance.insert(r1);

        assertEquals(r3, instance.poll());
        assertEquals(r2, instance.poll());
        assertEquals(r1, instance.poll());
    }

    @Test
    public void correctPriorityTest() {
        DataState d1 = new DataState("d1");
        DataState d2 = new DataState("d2");
        DataState d3 = new DataState("d3");
        DataState d4 = new DataState("d4");

        Map<State, Integer> heuristic = new HashMap<>();
        heuristic.put(d1, 3);
        heuristic.put(d2, 1);
        heuristic.put(d3, 3);
        heuristic.put(d4, 1);
        Run r1 = new Run(Node.EMPTY_NODE, d1, null, 0, 0);
        Run r2 = new Run(Node.EMPTY_NODE, d2, null, 0, 2);
        Run r3 = new Run(Node.EMPTY_NODE, d3, null, 0, 2);
        Run r4 = new Run(Node.EMPTY_NODE, d4, null, 0, 4);

        HeuristicPriorityQueue instance = new HeuristicPriorityQueue(heuristic);
        instance.insert(r1);
        instance.insert(r3);
        instance.insert(r4);
        instance.insert(r2);

        assertEquals(r1, instance.poll());
        assertEquals(r2, instance.poll());
        assertEquals(r3, instance.poll());
        assertEquals(r4, instance.poll());
    }

    @Test
    public void loopTest() {
        DataState d1 = new DataState("d1");
        DataState d2 = new DataState("d2");

        Map<State, Integer> heuristic = new HashMap<>();
        heuristic.put(d1, 3);
        heuristic.put(d2, 1);
        Run r1 = new Run(Node.EMPTY_NODE, d1, null, 0, 0);
        Run r2 = new Run(Node.EMPTY_NODE, d2, null, 0, 2);
        Run r3 = new Run(Node.EMPTY_NODE, d2, null, 0, 4);
        Run r4 = new Run(Node.EMPTY_NODE, d2, null, 0, 6);

        HeuristicPriorityQueue instance = new HeuristicPriorityQueue(heuristic);
        instance.insert(r1);
        instance.insert(r4);
        instance.insert(r3);
        instance.insert(r2);

        assertEquals(r1, instance.poll());
        assertEquals(r2, instance.poll());
        assertEquals(r3, instance.poll());
        assertEquals(r4, instance.poll());
    }
}
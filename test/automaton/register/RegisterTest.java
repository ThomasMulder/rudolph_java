package automaton.register;

import org.junit.Test;

import static org.junit.Assert.*;

public class RegisterTest {

    @Test
    public void equals() {
        Register r1 = new Register(1);
        int[] k = {-1};
        Register r2 = new Register(k);
        Register r3 = new Register(2);
        int[] l = {-1, 0};
        Register r4 = new Register(l);
        assertEquals(true, r1.equals(r1));
        assertEquals(true, r1.equals(r2));
        assertEquals(false, r1.getRegister().length == r3.getRegister().length);
        assertEquals(false, r1.equals(r3));
        assertEquals(true, r3.getRegister().length == r4.getRegister().length);
        assertEquals(false, r3.equals(r4));
    }
}
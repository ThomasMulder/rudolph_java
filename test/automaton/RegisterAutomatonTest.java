package automaton;

import automaton.state.State;
import automaton.transition.Transition;
import automaton.transition.TransitionSet;
import graph.Edge;
import graph.Graph;
import graph.Node;
import org.junit.Test;
import util.RegisterAutomatonBuilder;
import util.Pair;

import java.util.Map;

import static org.junit.Assert.*;

public class RegisterAutomatonTest {
    private RegisterAutomatonBuilder registerAutomatonBuilder = new RegisterAutomatonBuilder();

    @Test
    public void toGraph() {
        toGraphGivenQuery("b", 4, 3);
        toGraphGivenQuery("a before a before b before a before a", 12, 11);
        toGraphGivenQuery("a or b", 7, 6);
        toGraphGivenQuery("repeat(a)", 4, 4);
    }

    @Test
    public void equals() {
        String s1 = "c";
        String s2 = "repeat(((c) or (c))) or null before register(0,1) a if(regeq(0) && regeq(1))";
        RegisterAutomaton r1 = registerAutomatonBuilder.build(s1);
        RegisterAutomaton r2 = registerAutomatonBuilder.build(s1);
        RegisterAutomaton r3 = registerAutomatonBuilder.build(s2);
        RegisterAutomaton r4 = registerAutomatonBuilder.build(s2);
        RegisterAutomaton r5 = registerAutomatonBuilder.build("repeat(((c) or (c))) or null before register(0,1) a if(regeq(0) || regeq(1))");
        assertEquals(true, r1.equals(r2));
        assertEquals(true, r3.equals(r4));
        assertEquals(false, r3.equals(r5));
    }

    private void toGraphGivenQuery(String query, int expectedNodes, int expectedEdges) {
        RegisterAutomaton r = registerAutomatonBuilder.build(query);
        Pair<Graph, Map<State, Node>> p = r.toGraph();
        Graph g = p.getKey();
        Map<State, Node> stateNodeMap = p.getValue();

        assertEquals(expectedNodes, g.getNodes().size());
        assertEquals(expectedEdges, g.getEdges().size());
        assertEquals(true, containsEquivalentEdges(g, r.getDataTransitions(), stateNodeMap));
        assertEquals(true, containsEquivalentEdges(g, r.getWordTransitions(), stateNodeMap));
    }

    private boolean containsEquivalentEdges(Graph g, TransitionSet transitions, Map<State, Node> stateNodeMap) {
        for (Transition transition : transitions.getTransitions()) {
            Node source = stateNodeMap.get(transition.getSource());
            Node target = stateNodeMap.get(transition.getTarget());
            boolean containsEquivalentEdge = false;
            for (Edge edge : g.getEdges()) {
                if (edge.getSource().equals(source) && edge.getTarget().equals(target)) {
                    containsEquivalentEdge = true;
                    break;
                }
            }
            if (!containsEquivalentEdge) return false;
        }
        return true;
    }
}
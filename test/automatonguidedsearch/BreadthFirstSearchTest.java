package automatonguidedsearch;

import graph.Graph;
import org.junit.Test;
import query.Query;

import java.io.File;

public class BreadthFirstSearchTest extends AutomatonGuidedSearchTest {
    private BreadthFirstSearch instance = new BreadthFirstSearch();

    @Override
    protected AutomatonGuidedSearch getInstance() {
        return instance;
    }

//    @Test
//    public void anotherTest() {
//        // 12,39,13,51,3,18,24,85,8,5,45	8,59,56,33,75,33,17,33,64,37,25	repeat(((b))) or null
//        String path = "test" + File.separator + "automatonguidedsearch" + File.separator + "input" + File.separator + "genGraph2.csv";
//        Graph g = graphBuilder.build(false, path);
//        Query query = new Query("repeat(((b))) or null", g, "12,39,13,51,3,18,24,85,8,5,45", "8,59,56,33,75,33,17,33,64,37,25");
//        System.out.println(query.evaluate(instance, null));
//    }
}
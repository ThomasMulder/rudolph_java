package automatonguidedsearch;

import graph.Graph;
import org.junit.Test;
import query.Query;

import java.io.File;

public class DepthFirstSearchTest extends AutomatonGuidedSearchTest {
    private DepthFirstSearch instance = new DepthFirstSearch();

    @Override
    protected AutomatonGuidedSearch getInstance() {
        return instance;
    }

    @Test
    public void someTest() {
        // 28,12,21,69	?t	repeat(((d) or (b))) or null
        String path = "test" + File.separator + "automatonguidedsearch" + File.separator + "input" + File.separator + "genGraph.csv";
        Graph g = graphBuilder.build(false, path);
        Query query = new Query("repeat(((d) or (b))) or null", g, "28", "?t");
        query.evaluate(instance, null);
    }

//    @Test
//    public void anotherTest() {
//        // 12,39,13,51,3,18,24,85,8,5,45	8,59,56,33,75,33,17,33,64,37,25	repeat(((b))) or null
//        String path = "test" + File.separator + "automatonguidedsearch" + File.separator + "input" + File.separator + "genGraph2.csv";
//        Graph g = graphBuilder.build(false, path);
//        Query query = new Query("repeat(((b))) or null", g, "12,39,13,51,3,18,24,85,8,5,45", "8,59,56,33,75,33,17,33,64,37,25");
//        System.out.println(query.evaluate(instance, null));
//    }
}
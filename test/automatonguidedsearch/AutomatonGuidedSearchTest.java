package automatonguidedsearch;

import automaton.RegisterAutomaton;
import graph.Graph;
import graph.GraphBuilder;
import graph.Node;
import org.junit.Test;
import query.Query;
import util.RegisterAutomatonBuilder;
import util.Pair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public abstract class AutomatonGuidedSearchTest {
    protected GraphBuilder graphBuilder = new GraphBuilder();
    protected RegisterAutomatonBuilder registerAutomatonBuilder = new RegisterAutomatonBuilder();
    protected String graphPath = "test" + File.separator + "automatonguidedsearch" + File.separator + "input" + File.separator + "graph.tsv";
    protected Graph graph = graphBuilder.build(true, graphPath);
    protected List<Node> sources = new ArrayList<>();
    protected List<Node> targets = new ArrayList<>();

    public AutomatonGuidedSearchTest() {
        this.sources.add(graph.getNodeByLabel("u"));
        this.targets.add(graph.getNodeByLabel("v"));
    }

    protected abstract AutomatonGuidedSearch getInstance();

    protected void assertQueries(String[] expectations, String[] queries) {
        if (expectations.length != queries.length) throw new IllegalArgumentException("Must have an expectation for each query, and a query for each expectation");
        for (int i = 0; i < expectations.length; i++) {
            assertQuery(expectations[i], registerAutomatonBuilder.build(queries[i]));
        }
    }

    protected void assertQuery(String expected, RegisterAutomaton r) {
        String actual = getInstance().perform(graph, sources, targets, r, null);
        if (expected == null || actual == null) {
            assertEquals(expected, actual);
        }
        else if (expected != null && expected.equals("-")) {
            assertNotEquals(null, actual);
        } else {
            String minimized = minimizePath(actual);
            assertTrue(expected.equals(actual) || expected.equals(minimized));
        }
    }

    private String minimizePath(String path) {
        String[] labels = path.split("/");
        String result = "";
        Pair<Integer, Integer> indices = pathContainsCycles(labels);
        int i = indices.getKey();
        int j = indices.getValue();
        if (i + j == -2) return path;
        while (!(i + j == -2)) {
            result = "";
            for (int k = 0; k < labels.length; k++) {
                if (k <= i || k > j) {
                    result = result.concat(labels[k] + "/");
                }
            }
            labels = result.split("/");
            indices = pathContainsCycles(labels);
            i = indices.getKey();
            j = indices.getValue();
        }
        return result.substring(0, result.length() - 1);
    }

    private Pair<Integer, Integer> pathContainsCycles(String[] labels) {
        for (int i = 0; i < labels.length - 1; i++) {
            for (int j = i + 1; j < labels.length; j++) {
                if (labels[i].equals(labels[j])) return new Pair(i, j);
            }
        }
        return new Pair(-1, -1);
    }

    @Test
    public void simplePathTest() {
        String q1 = "a before b before c";
        String q2 = "d before d before c";
        String q3 = "b before b before c";
        String[] e = {"u/n1/n3/v", "u/n2/n3/v", null};
        String[] q = {q1, q2, q3};

        assertQueries(e, q);
    }

    @Test
    public void disjunctivePathTest() {
        String q1 = "a before b before (a or b or c)";
        String q2 = "(a before b) or (d before d) before c";
        String q3 = "(a or d) before d before c";
        String q4 = "(a or b or d) before (a or b or d) before (a or b or d)";
        String[] e = {"u/n1/n3/v", "-", "u/n2/n3/v", null};
        String[] q = {q1, q2, q3, q4};

        assertQueries(e, q);
    }

    @Test
    public void redundantDisjunctionsTest() {
        String path = "test" + File.separator + "automatonguidedsearch" + File.separator + "input" + File.separator + "genGraph.csv";
        Graph g = graphBuilder.build(false, path);
        Query q = new Query("repeat(((c) or (c))) or null", g, "29", "?t");
        String actual = q.evaluate(getInstance(), null);
        assertEquals("29/957", actual);
    }

    @Test
    public void loopTest() {
        String q1 ="a before a before a before a before b before c";
        String q2 ="repeat(a) before b before c";
        String q3 ="repeat(a) before a before b before c";
        String q4 ="repeat(a) before a if(eq(1)) before repeat(a) before b before c";
        String q5 = "a before repeat(a if(eq(2))) before b before c";
        String[] e = {"u/n1/n4/n5/n1/n3/v", "u/n1/n3/v", "-", "-", null};
        String[] q = {q1, q2, q3, q4, q5};

        assertQueries(e, q);
    }

    @Test
    public void eventualityTest() {
        String q1 = "(repeat(a or c) or null) before b before (repeat(a or c) or null)";
        String q2 = "(repeat(c or e) or null) before d before (repeat(c or d) or null)";
        String q3 = "(repeat(c or e) or null) before d before (repeat(a or b) or null)";
        String[] e = {"u/n1/n3/v", "u/n2/n3/v", null};
        String[] q = {q1, q2, q3};

        assertQueries(e, q);
    }

    @Test
    public void dataPathTest() {
        String q1 = "register(0) (a or d) if(regeq(0)) before repeat(a or b or c or d)";
        String q2 = "register(0) (a or d) if(regneq(0)) before repeat(a or b or c or d)";
        String q3 = "repeat((a or b or c or d) if(eq(1)))";
        String q4 = "repeat((a or b or c or d) if(eq(1) || eq(2)))";
        String[] e = {"u/n1/n3/v", "u/n2/n3/v", null, "-"};
        String[] q = {q1, q2, q3, q4};

        assertQueries(e, q);
    }
}
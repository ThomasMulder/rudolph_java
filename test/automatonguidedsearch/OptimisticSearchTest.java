package automatonguidedsearch;

import graph.Edge;
import graph.Graph;
import graph.Node;
import org.junit.Test;
import query.Query;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class OptimisticSearchTest extends AutomatonGuidedSearchTest {
    private OptimisticSearch instance = new OptimisticSearch();


    @Override
    protected AutomatonGuidedSearch getInstance() {
        return instance;
    }

    @Test
    public void shorterDisjunctionFirstTest() {
        String graphPath = "test" + File.separator + "automatonguidedsearch" + File.separator + "input" + File.separator + "largeGraph.csv";
        Graph graph = graphBuilder.build(true, graphPath);
        String queryString = "(b before b before a before a before c before b before c before d before d before a before d before d before a before d before a before b before b before a before e) or (f before c before d before f before d before d before b before b before b before f before f before a before d before e before c before d before a before d before a before e before f before f before a before a before e before c before a before a before f before e before c before d before e)";

        //        Node u = new Node("u",1);
//        Node x = new Node("x",1);
//        Node y = new Node("y", 1);
//        Node z = new Node("z", 1);
//        Node v = new Node("v", 1);
//        Edge ux = new Edge(u, x, "a");
//        Edge xv = new Edge(x, v, "a");
//        Edge uy = new Edge(u, y, "b");
//        Edge yz = new Edge(y, z, "b");
//        Edge zv = new Edge(z, v, "b");
//        Graph g = graphBuilder.build(true, ux, xv, uy, yz, zv);

        Query q = new Query(queryString, graph, "73664", "?t");
        List<String> statistics = new ArrayList<>();
        String result = q.evaluate(instance, statistics);
        assertFalse(statistics.isEmpty());
    }
}
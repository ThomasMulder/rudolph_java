package query;

import org.junit.Test;
import util.RegisterAutomatonBuilder;

import static org.junit.Assert.*;

public class RegisterAutomatonBuilderTest {
    private RegisterAutomatonBuilder instance = new RegisterAutomatonBuilder();

    @Test
    public void zeroRegistersTest() throws Exception {
        String query = "a before b";
        assertEquals(0, instance.countRegisterUsage(query));
    }

    @Test
    public void oneRegisterTest() throws Exception {
        String query = "a before b register(0) before c";
        assertEquals(1, instance.countRegisterUsage(query));
    }

    @Test
    public void multipleRegistersTest() throws Exception {
        String query = "a before b register(0,1) before c";
        assertEquals(2, instance.countRegisterUsage(query));
        query = "a before b register(0) before c register(1)";
        assertEquals(2, instance.countRegisterUsage(query));
    }

    @Test
    public void duplicateRegistersTest() throws Exception {
        String query = "a before b register(0) before c register(0)";
        assertEquals(1, instance.countRegisterUsage(query));
    }
}
package query;

import graph.Edge;
import graph.Graph;
import graph.GraphBuilder;
import graph.Node;
import org.junit.Test;
import util.Pair;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class QueryTest {
    private static final GraphBuilder graphBuilder = new GraphBuilder();
    private static final String queryString = "a before a";

//    @Test
//    public void sourceDoesNotBelongToGraphTest() {
//        try {
//            new Query(queryString, new Graph(), "n1", "n2").evaluate(null, null);
//            fail("Expected an IllegalArgumentException to be thrown");
//        } catch (IllegalArgumentException exception) {
//            assertThat(exception.getMessage(), is("Source does not belong to graph."));
//        }
//    }

//    @Test
//    public void targetDoesNotBelongToGraphTest() {
//        Graph graph = new Graph();
//        Node n1 = new Node("n1", 1);
//        graph.addNode(n1);
//        try {
//            new Query(queryString, graph, "n1", "n2").evaluate(null, null);
//            fail("Expected an IllegalArgumentException to be thrown");
//        } catch (IllegalArgumentException exception) {
//            assertThat(exception.getMessage(), is("Target does not belong to graph."));
//        }
//
//    }

    @Test
    public void singleSourceSingleTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "u", "v");

        String[] expectedSourceLabels = {"u"};
        String[] expectedTargetLabels = {"v"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("u", "v");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void multiSourceSingleTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "u,x", "v");

        String[] expectedSourceLabels = {"u", "x"};
        String[] expectedTargetLabels = {"v"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("u,x", "v");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void singleSourceMultiTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "u", "x,v");

        String[] expectedSourceLabels = {"u"};
        String[] expectedTargetLabels = {"x", "v"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("u", "x,v");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void multiSourceMultiTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "u,x", "x,v");

        String[] expectedSourceLabels = {"u", "x"};
        String[] expectedTargetLabels = {"x", "v"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("u,x", "x,v");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void singleSourceArbitraryTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "u", "?t");

        String[] expectedSourceLabels = {"u"};
        String[] expectedTargetLabels = {"?"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("u", "?t");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void arbitrarySourceSingleTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "?s", "v");

        String[] expectedSourceLabels = {"u", "x", "v"};
        String[] expectedTargetLabels = {"v"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("?s", "v");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void multiSourceArbitraryTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "u,x", "?t");

        String[] expectedSourceLabels = {"u", "x"};
        String[] expectedTargetLabels = {"?"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("u,x", "?t");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void aribtrarySourceMultiTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "?s", "x,v");

        String[] expectedSourceLabels = {"u", "x", "v"};
        String[] expectedTargetLabels = {"x","v"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("?s", "x,v");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void sameArbitrarySourceAndTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "?s", "?s");

        String[] expectedSourceLabels = {"u", "x", "v"};
        String[] expectedTargetLabels = {};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("?s", "?s");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    @Test
    public void differentArbitrarySourceAndTargetTest() {
        Graph g = getGraph();
        Query q = new Query(queryString, g, "?s", "?t");

        String[] expectedSourceLabels = {"u", "x", "v"};
        String[] expectedTargetLabels = {"?"};
        Pair<String[], String[]> actualLabelsPair = q.processLabels("?s", "?t");

        assertTrue(labelsAreEqual(expectedSourceLabels, actualLabelsPair.getKey()));
        assertTrue(labelsAreEqual(expectedTargetLabels, actualLabelsPair.getValue()));
    }

    private boolean labelsAreEqual(String[] expectedLabels, String[] actualLabels) {
        if (expectedLabels.length != actualLabels.length) return false;
        for (String expectedLabel: expectedLabels) {
            boolean containedInActual = false;
            for (String actualLabel: actualLabels) {
                if (expectedLabel.equals(actualLabel)) {
                    containedInActual = true;
                    break;
                }
            }
            if (!containedInActual) return false;
        }
        return true;
    }

    private Graph getGraph() {
        Node u = new Node("u", 1);
        Node x = new Node("x", 1);
        Node v = new Node("v", 1);
        Edge ux = new Edge(u, x, "a");
        Edge xv = new Edge(x, v, "a");
        return graphBuilder.build(false, ux, xv);
    }
}
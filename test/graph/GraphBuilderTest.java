package graph;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;


public class GraphBuilderTest {
    private static final String inputPath = "test" + File.separator + "graph" + File.separator + "input" + File.separator;
    private GraphBuilder instance = new GraphBuilder();

    @Test
    public void buildSelfLoopAGraphFromFile() {
        String fileName = inputPath + "selfLoopAGraph.csv";
        Graph result = instance.build(true, fileName);
        assertEquals(1, result.getNodes().size());
        assertEquals(1, result.getEdges().size());
        Node node = result.getNodes().iterator().next();
        Edge edge = result.getEdges().iterator().next();
        assertEquals("u",node.getLabel());
        assertEquals(1, node.getValue());
        assertEquals("a", edge.getWord());
        assertEquals(node, edge.getSource());
        assertEquals(node, edge.getTarget());
    }

    @Test
    public void buildSimpleABGraphFromFile() {
        String fileName = inputPath + "simpleABGraph.csv";
        Graph result = instance.build(true, fileName);
        assertEquals(3, result.getNodes().size());
        assertEquals(2, result.getEdges().size());
        Node u = new Node("u", 1);
        Node x = new Node("x", 1);
        Node v = new Node("v", 1);
        Edge a = new Edge(u, x, "a");
        Edge b = new Edge(x, v, "b");
        assertEquals(u, result.getNodeByLabel("u"));
        assertEquals(x, result.getNodeByLabel("x"));
        assertEquals(v, result.getNodeByLabel("v"));
        assertEquals(a, result.getEdgeByLabels("u", "x"));
        assertEquals(b, result.getEdgeByLabels("x", "v"));
    }
}
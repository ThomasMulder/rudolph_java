package condition;


import automaton.condition.Condition;
import automaton.condition.EmptyCondition;
import automaton.condition.construction.rule.ConditionRule;
import automaton.condition.construction.rule.expand.NotRule;
import automaton.condition.construction.rule.merge.AndRule;
import automaton.condition.construction.rule.merge.OrRule;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConditionConstructionTest {

    @Test
    public void noNestedEmptyConditionsTest() {
        ConditionRule notRule = new NotRule();
        ConditionRule orRule = new OrRule();
        ConditionRule andRule = new AndRule();
        Condition emptyConditionOne = new EmptyCondition();
        Condition emptyConditionTwo = new EmptyCondition();
        Condition[] inputNot = {emptyConditionOne};
        Condition[] inputMerge = {emptyConditionOne, emptyConditionTwo};
        Condition result = notRule.apply(inputNot);
        assertEquals(emptyConditionOne, result);
        result = orRule.apply(inputMerge);
        assertEquals(emptyConditionOne, result);
        result = andRule.apply(inputMerge);
        assertEquals(emptyConditionTwo, result);
    }
}

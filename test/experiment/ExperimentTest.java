package experiment;

import graph.Graph;
import graph.GraphBuilder;
import org.junit.Test;
import query.Query;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

public class ExperimentTest {
    private Experiment instance = new Experiment();
    private GraphBuilder graphBuilder = new GraphBuilder();
    private static final String inputPath = "test" + File.separator + "experiment" + File.separator + "input" + File.separator;

    @Test
    public void gatherWorkloads() {
        Graph graph = graphBuilder.build(true, inputPath + "Graph.csv");
        List<List<Query>> workloads = instance.gatherWorkloads(inputPath + File.separator + "Workloads", graph);

        Query expectedQueryOne = new Query("a before b before c", graph, "u", "v");
        Query expectedQueryTwo = new Query("repeat(a or null) before b", graph, "x", "v");
        Query expectedQueryThree = new Query("c before b before a", graph, "x", "x");
        Query expectedQueryFour = new Query("repeat(b)", graph, "v", "u");

        assertEquals(2, workloads.size());
        assertEquals(2, workloads.get(0).size());
        assertEquals(2, workloads.get(1).size());
        assertEquals(expectedQueryOne, workloads.get(1).get(0));
        assertEquals(expectedQueryTwo, workloads.get(1).get(1));
        assertEquals(expectedQueryThree, workloads.get(0).get(0));
        assertEquals(expectedQueryFour, workloads.get(0).get(1));
    }
}
# README #

Rudolph is the code name for the implementation of Regular Path Query evaluation using automaton-guided breadth-first search
for the 2IMW00 Seminar of Q2 2017/2018 at the Eindhoven University of Technology.
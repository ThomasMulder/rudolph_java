package graph;

public class Edge {
    private Node source, target;
    private String word;

    public Edge(Node source, Node target, String word) {
        this.source = source;
        this.target = target;
        this.word = word;
        this.source.addOutgoingEdge(this);
        this.target.addIncomingEdge(this);
    }

    public Node getSource() {
        return source;
    }

    public Node getTarget() {
        return target;
    }

    public String getWord() {
        return word;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Edge)) return false;
        Edge edge = (Edge) object;
        return edge.getSource().equals(source) && edge.getTarget().equals(target);
    }
}

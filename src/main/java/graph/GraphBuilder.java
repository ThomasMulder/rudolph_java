package graph;

import java.io.*;
import java.util.*;

public class GraphBuilder {

    public GraphBuilder() {}

    public Graph build(boolean permute, Edge... edges) {
        Graph result = new Graph();
        for (Edge edge : edges) {
            result.addNode(edge.getSource());
            result.addNode(edge.getTarget());
            result.addEdge(edge);
        }
        if (permute) result.permute();
        return result;
    }

    public Graph build(boolean permute, String fileName) {
        Graph result = new Graph();
        Set<String> labels = new HashSet<>();
        Map<String, Node> nodeMap = new HashMap<>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File(fileName)));
            String line;
            int lineNumber = 0;
            while((line = br.readLine()) != null) {
                lineNumber++;
                StringTokenizer stringTokenizer = new StringTokenizer(line, "\t");
                String[] values = new String[stringTokenizer.countTokens()];
                int i = 0;
                while (stringTokenizer.hasMoreTokens()) {
                    values[i] = stringTokenizer.nextToken();
                    i++;
                }
                if (values.length == 2) { // Node
                    String label = values[0];
                    if (labels.contains(label)) throw new IllegalStateException("Node at line " + lineNumber + " is not unique.");
                    labels.add(label);
                    int value = Integer.valueOf(values[1]);
                    Node node = new Node(label, value);
                    result.addNode(node);
                    nodeMap.put(label, node);
                } else if (values.length == 3) { // Edge
                    Node source = nodeMap.get(values[0]);
                    if (source == null) throw new IllegalStateException("Source node referenced at line " + lineNumber + " does not exist.");
                    Node target = nodeMap.get(values[1]);
                    if (target == null) throw new IllegalStateException("Target node referenced at line " + lineNumber + " does not exist.");
                    String word = values[2];
                    result.addEdge(new Edge(source, target, word));
                } else {
                    throw new IllegalStateException("Invalid type at line " + lineNumber);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (permute) result.permute();
        return result;
    }
}

package graph;

import java.util.*;

public class Graph {
    private Set<Node> nodes;
    private Set<Edge> edges;

    public static final Node ARBITRARY_NODE = new Node("?", 0);

    public Graph() {
        this.nodes = new HashSet<>();
        this.edges = new HashSet<>();
    }

    public Set<Node> getNodes() {
        return nodes;
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    public void addNode(Node node) {
        this.nodes.add(node);
    }

    public void addEdge(Edge edge) {
        this.edges.add(edge);
    }

    public Node getNodeByLabel(String label) {
        for (Node node : nodes) {
            if (label.equals("?")) return ARBITRARY_NODE;
            if (node.getLabel().equals(label)) return node;
        }
        return null;
    }

    public List<Node> getNodesByLabels(String[] labels) {
        List<Node> result = new ArrayList<>();
        for (String label : labels) {
            result.add(getNodeByLabel(label));
        }
        return result;
    }

    public Edge getEdgeByLabels(String sourceLabel, String targetLabel) {
        for (Edge edge : edges) {
            if (edge.getSource().getLabel().equals(sourceLabel) && edge.getTarget().getLabel().equals(targetLabel)) return edge;
        }
        return null;
    }

    /**
     * Changes the order of the out-going edges of each node to a random permutation.
     */
    public void permute() {
        Random random = new Random();
        for (Node node : nodes) {
            List<Edge> original = node.getOutgoingEdges();
            List<Edge> permutation = new ArrayList<>();
            while (!original.isEmpty()) {
                int index = random.nextInt(original.size());
                permutation.add(original.get(index));
                original.remove(index);
            }
            node.setOutgoingEdges(permutation);
        }
    }
}

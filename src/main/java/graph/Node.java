package graph;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private String label;
    private int value;
    private List<Edge> outgoingEdges, incomingEdges;

    public static final Node EMPTY_NODE = new Node("null", 0);

    public Node(String label, int value) {
        this.label = label;
        this.value = value;
        this.outgoingEdges = new ArrayList<>();
        this.incomingEdges = new ArrayList<>();
    }

    public String getLabel() {
        return label;
    }

    public int getValue() {
        return value;
    }

    public List<Edge> getOutgoingEdges() {
        return outgoingEdges;
    }

    public List<Edge> getIncomingEdges() {
        return incomingEdges;
    }

    public void addOutgoingEdge(Edge edge) {
        this.outgoingEdges.add(edge);
    }

    public void addIncomingEdge(Edge edge) {
        this.incomingEdges.add(edge);
    }

    public void setOutgoingEdges(List<Edge> edges) {
        outgoingEdges = edges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        if (value != node.value) return false;
        return label.equals(node.label);

    }
}

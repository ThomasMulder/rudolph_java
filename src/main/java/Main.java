import automatonguidedsearch.*;
import experiment.Experiment;

public class Main {
    private static final AutomatonGuidedSearch[] searchStrategies = {new BreadthFirstSearch(), new DepthFirstSearch(), new OptimisticSearch()};

    public static void main(String[] args) {
        if (args.length != 4) throw new IllegalArgumentException("No experiment id, directory path, evaluation strategy indicator or query reuse indicator was specified.");
        Experiment experiment = new Experiment();
        String experimentId = args[0];
        String experimentDirectory = args[1];
        int strategyIndex = Integer.valueOf(args[2]);
        boolean reuseQuery = Boolean.parseBoolean(args[3]);
        experiment.perform(experimentId, experimentDirectory, searchStrategies[strategyIndex], reuseQuery);
    }
}

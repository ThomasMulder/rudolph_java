package query;

import automaton.RegisterAutomaton;
import automatonguidedsearch.AutomatonGuidedSearch;
import graph.Graph;
import graph.Node;
import util.Pair;
import util.RegisterAutomatonBuilder;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.util.*;

public class Query {
    private String queryString;
    private Graph graph;
    private List<Node> sources, targets;
    private RegisterAutomaton registerAutomaton;

    public Query(String queryString, Graph graph, String sourceLabelString, String targetLabelString) {
        this.queryString = queryString;
        this.graph = graph;
        this.sources = new LinkedList<>();
        this.targets = new LinkedList<>();
        Pair<String[], String[]> sourceAndTargetLabels = processLabels(sourceLabelString, targetLabelString);
        String[] sourceLabels = sourceAndTargetLabels.getKey();
        String[] targetLabels = sourceAndTargetLabels.getValue();
        this.sources.addAll(graph.getNodesByLabels(sourceLabels));
        if (targetLabels != null) {
            this.targets.addAll(graph.getNodesByLabels(targetLabels));
        }
        this.registerAutomaton = new RegisterAutomatonBuilder().build(queryString);
    }

    public Query(RegisterAutomaton registerAutomaton, Graph graph, String sourceLabelString, String targetLabelString) {
        this.graph = graph;
        this.sources = new LinkedList<>();
        this.targets = new LinkedList<>();
        Pair<String[], String[]> sourceAndTargetLabels = processLabels(sourceLabelString, targetLabelString);
        String[] sourceLabels = sourceAndTargetLabels.getKey();
        String[] targetLabels = sourceAndTargetLabels.getValue();
        this.sources.addAll(graph.getNodesByLabels(sourceLabels));
        if (targetLabels != null) {
            this.targets.addAll(graph.getNodesByLabels(targetLabels));
        }
        this.registerAutomaton = registerAutomaton;
    }

    public String evaluate(AutomatonGuidedSearch searchStrategy, List<String> statistics) {
        // Statistics gathering
        ManagementFactory.getMemoryMXBean().gc();
        long initialMemoryUsage = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
        long startTime = System.nanoTime();
        int maxIntermediateSize = 0;
        int totalIntermediateSize = 0;

        String result = searchStrategy.perform(graph, sources, targets, getRegisterAutomaton(), statistics);
        Pair<Integer, Integer> intermediates = updateStatistics(statistics, initialMemoryUsage, startTime, maxIntermediateSize, totalIntermediateSize);
        maxIntermediateSize = intermediates.getKey();
        totalIntermediateSize = intermediates.getValue();
        if (result != null) {
            return result;
        }
        updateStatistics(statistics, initialMemoryUsage, startTime, maxIntermediateSize, totalIntermediateSize);
        return null;
    }

    private Pair<Integer, Integer> updateStatistics(List<String> statistics, long initialMemoryUsage, long startTime, int maxIntermediateSize, int totalIntermediateSize) {
        if (statistics != null) {
            int currentMaxIntermediateSize = -1;
            int currentTotalIntermediateSize = -1;
            try {
                currentMaxIntermediateSize = Integer.parseInt(statistics.get(0));
                currentTotalIntermediateSize = Integer.parseInt(statistics.get(1));
            } catch (NumberFormatException e) {}
            if (currentMaxIntermediateSize > maxIntermediateSize) maxIntermediateSize = currentMaxIntermediateSize;
            totalIntermediateSize += currentTotalIntermediateSize;
            statistics.clear();
            statistics.add(String.valueOf(calculateEvaluationTime(startTime)));
            statistics.add(String.valueOf(calculateMemoryUsage(initialMemoryUsage)));
            statistics.add(String.valueOf(maxIntermediateSize));
            statistics.add(String.valueOf(totalIntermediateSize));
        }
        return new Pair(maxIntermediateSize, totalIntermediateSize);
    }

    public Pair<String[], String[]> processLabels(String sourceLabelString, String targetLabelString) {
        String[] sourceLabels = sourceLabelString.split(",");
        String[] targetLabels = targetLabelString.split(",");
        String arbitrarySourceLabel = findFirstArbitraryNodeLabel(sourceLabels);
        String arbitraryTargetLabel = findFirstArbitraryNodeLabel(targetLabels);
        if (arbitrarySourceLabel == null && arbitraryTargetLabel == null) { // No arbitrary source or target label
            return new Pair(sourceLabels, targetLabels);
        }

        if (arbitrarySourceLabel == null) { // Only an arbitrary target label
            targetLabels = new String[1];
            targetLabels[0] = "?";
            return new Pair(sourceLabels, targetLabels);
        }

        sourceLabels = new String[graph.getNodes().size()];
        int i = 0;
        for (Node node : graph.getNodes()) {
            sourceLabels[i] = node.getLabel();
            i++;
        }
        if (arbitraryTargetLabel == null) { // Only an arbitrary source label
            return new Pair(sourceLabels, targetLabels);
        }

        if (arbitrarySourceLabel.equals(arbitraryTargetLabel)) { // Same arbitrary source and target label
           return new Pair(sourceLabels, new String[0]);
        }

        // Different arbitrary source and target labels
        int n = graph.getNodes().size();
        sourceLabels = new String[n];
        targetLabels = new String[1];
        targetLabels[0] = "?";
        i = 0;
        for (Node node : graph.getNodes()) {
            sourceLabels[i] = node.getLabel();
            i++;
        }
        return new Pair(sourceLabels, targetLabels);
    }

    private String findFirstArbitraryNodeLabel(String[] labels) {
        for (String label : labels) {
            if (label.contains("?")) return label.substring(1);
        }
        return null;
    }

    private long calculateMemoryUsage(long initialMemoryUsage) {
        long result = 0;
        for (MemoryPoolMXBean poolMXBean : ManagementFactory.getMemoryPoolMXBeans()) {
            String name = poolMXBean.getName();
            if (name.equals("G1 Eden Space") || name.equals("G1 Old Gen") || name.equals("G1 Survivor Space")) {
                result += poolMXBean.getPeakUsage().getUsed();
            }
        }
        return result - initialMemoryUsage;
    }

    private long calculateEvaluationTime(long startTime) {
        return (System.nanoTime() - startTime) / ((long) 1e6);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Query query = (Query) o;
        return Objects.equals(queryString, query.queryString) &&
                Objects.equals(graph, query.graph) &&
                Objects.equals(sources, query.sources) &&
                Objects.equals(targets, query.targets);
    }

    @Override
    public String toString() {
        String result = "";
        for (Node source : sources) {
            result = result.concat(source.getLabel() + "|");
        }
        result = result.substring(0, result.length() - 1);
        result = result.concat("\t");
        for (Node target : targets) {
            result = result.concat(target.getLabel() + "|");
        }
        result = result.substring(0, result.length() - 1);
        result = result.concat("\t");
        return result.concat(queryString);
    }

    public RegisterAutomaton getRegisterAutomaton() {
        return registerAutomaton;
    }
}

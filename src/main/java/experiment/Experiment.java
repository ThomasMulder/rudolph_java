package experiment;

import automaton.RegisterAutomaton;
import automatonguidedsearch.AutomatonGuidedSearch;
import automatonguidedsearch.DepthFirstSearch;
import automatonguidedsearch.OptimisticSearch;
import graph.Graph;
import graph.GraphBuilder;
import query.Query;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Experiment {
    private static final String GRAPH_FILE_NAME = "Graph.csv";
    private static final String WORKLOAD_DIR_NAME = "Workloads";
    private static final String RESULTS_DIR_NAME = "Results";
    private static GraphBuilder graphBuilder = new GraphBuilder();

    public void perform(String experimentId, String experimentDirectoryPath, AutomatonGuidedSearch searchStrategy, boolean reuseQuery) {
        if (!isValidExperiment(experimentDirectoryPath)) throw new IllegalArgumentException("Invalid experimental setup.");
        new File(experimentDirectoryPath + File.separator + RESULTS_DIR_NAME).mkdir();

	    System.out.println("Building graph...");
        Graph graph = graphBuilder.build(true,experimentDirectoryPath + File.separator + GRAPH_FILE_NAME);
	
	    System.out.println("Gathering workloads...");
        List<List<Query>> workloads = gatherWorkloads(experimentDirectoryPath + File.separator + WORKLOAD_DIR_NAME, graph, reuseQuery);

        int i = 0;
        for (List<Query> workload : workloads) {
		    System.out.println("Processing workload " + String.valueOf(i + 1) + "/" + String.valueOf(workloads.size()));
            File out = new File(experimentDirectoryPath + File.separator + RESULTS_DIR_NAME + File.separator + "workload" + String.valueOf(i) + "_" + searchStrategy.getShortName() + ".txt");
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(out);
                String strategyInfo = "Evaluation strategy = " + searchStrategy.getName() + "\n";
                outputStream.write(strategyInfo.getBytes());
                outputStream.write("search strategy flag, experiment id, query, query result, total size of intermediate result(# runs), maximal size of intermediate result (# runs), memory usage (B), evaluation time (ms)\n".getBytes());
                int strategyIndex = 0;
                if (searchStrategy instanceof DepthFirstSearch) strategyIndex = 1;
                if (searchStrategy instanceof OptimisticSearch) strategyIndex = 2;
                byte[] searchStategyFlagB = (String.valueOf(strategyIndex) + ",").getBytes();
                byte[] experimentIdB = (experimentId + ",").getBytes();
		        int k = 0;
                for (Query query : workload) {
                    List<String> statistics = new ArrayList<>();
			        System.out.println("Evaluating query " + String.valueOf(k + 1) + "/" + String.valueOf(workload.size()));
                    statistics.add(String.valueOf(query.evaluate(searchStrategy, statistics)));
                    byte[] queryB = (query.toString() + ",").getBytes();
                    outputStream.write(searchStategyFlagB);
                    outputStream.write(experimentIdB);
                    outputStream.write(queryB);
                    for (int j = statistics.size() - 1; j >= 0; j--) {
                        byte[] result = String.valueOf(statistics.get(j)).getBytes();
                        outputStream.write(result);
                        if (j > 0) outputStream.write(",".getBytes());
                        if (j == 0) outputStream.write("\n".getBytes());
                    }
		            k++;
                }
                outputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            i++;
        }
    }

    public List<List<Query>> gatherWorkloads(String workloadDirectoryPath, Graph graph, boolean reuseQuery) {
        List<List<Query>> result = new ArrayList<>();
        File workloadDirectory = new File(workloadDirectoryPath);
        for (File workloadFile : workloadDirectory.listFiles()) {
		System.out.println("Processing file " + workloadFile.getName() + "...");
            result.add(buildWorkload(workloadFile, graph, reuseQuery));
        }
        return result;
    }

    private List<Query> buildWorkload(File workloadFile, Graph graph, boolean reuseQuery) {
        List<Query> result = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(workloadFile);
            RegisterAutomaton registerAutomaton = null;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] labels = line.split("\t");
                String query = line.substring(labels[0].length() + labels[1].length() + 2);
		        System.out.println("Gathering new query...");
		        if (reuseQuery) {
		            if (registerAutomaton == null) {
		                Query q = new Query(query, graph, labels[0], labels[1]);
		                registerAutomaton = q.getRegisterAutomaton();
		                result.add(q);
                    } else {
		                result.add(new Query(registerAutomaton, graph, labels[0], labels[1]));
                    }
                } else {
                    result.add(new Query(query, graph, labels[0], labels[1]));
                }
		        System.out.println(String.valueOf(result.size()) + " queries gathered");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    private boolean isValidExperiment(String experimentDirectory) {
        File file = new File(experimentDirectory);
        if (!file.exists() || !file.isDirectory()) return false;
        File[] experimenFiles = file.listFiles();
        if (!containsGraph(experimenFiles)) return false;
        return containsWorkload(experimenFiles);
    }

    private boolean containsWorkload(File[] files) {
        for (File file : files) {
            if (file.isDirectory() && file.getName().equals(WORKLOAD_DIR_NAME)) {
                File[] workloadFiles = file.listFiles();
                return workloadFiles.length > 0;
            }
        }
        return false;
    }

    private boolean containsGraph(File[] files) {
        for (File file : files) {
            if (file.getName().equals(GRAPH_FILE_NAME)) return true;
        }
        return false;
    }
}

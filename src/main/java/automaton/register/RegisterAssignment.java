package automaton.register;

import java.util.*;

public class RegisterAssignment {
    private Set<Integer> assignment;

    public RegisterAssignment(int... assignment) {
        this.assignment = new HashSet<>();
        for (int i : assignment) {
            this.assignment.add(i);
        }
    }

    public RegisterAssignment union(RegisterAssignment registerAssignment) {
        this.assignment.addAll(registerAssignment.assignment);
        return this;
    }

    public Set<Integer> getAssignment() {
        return assignment;
    }

    @Override
    public String toString() {
        String result = "[";
        for (int a : assignment) {
            result = result.concat(String.valueOf(a) + ",");
        }
        result.substring(0, result.length() - 1);
        result = result.concat("]");
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterAssignment that = (RegisterAssignment) o;
        for (int i : assignment) {
            boolean contained = false;
            for (int j : that.assignment) {
                if (i == j) {
                    contained = true;
                    break;
                }
            }
            if (!contained) return false;
        }
        return true;
    }
}

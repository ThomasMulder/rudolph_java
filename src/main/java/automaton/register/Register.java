package automaton.register;

import java.util.Arrays;

public class Register {
    private int[] register;

    public Register(int k) {
        this.register = new int[k];
        initialise();
    }

    public Register(int[] initialRegister) {
        this.register = new int[initialRegister.length];
        initialise(initialRegister);
    }

    public Register apply(RegisterAssignment registerAssignment, int value) {
        Register result = new Register(register.length);
        for (int i = 0; i < register.length; i++) {
            result.register[i] = register[i];
        }
        for (int i : registerAssignment.getAssignment()) {
            result.register[i] = value;
        }
        return result;
    }

    public int[] getRegister() {
        return register;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Register register1 = (Register) o;
        return Arrays.equals(register, register1.register);
    }

    private void initialise() {
        for (int i = 0; i < register.length; i++) {
            register[i] = -1;
        }
    }

    private void initialise(int[] register) {
        for (int i = 0; i < register.length; i++) {
            this.register[i] = register[i];
        }
    }
}

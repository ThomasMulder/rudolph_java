package automaton.state;

import automaton.register.Register;
import automaton.transition.DataTransition;
import automaton.transition.WordTransition;

import java.util.Set;

public interface State {

    Set<WordTransition> getWordTransitions();

    Set<DataTransition> getDataTransitions();

    String getLabel();

    State copy();

    StateStatus getStatus();

    void setStatus(StateStatus status);

    void addWordTransition(WordTransition wordTransition);

    void addDataTransition(DataTransition dataTransition);

    void removeWordTransition(WordTransition wordTransition);

    void removeDataTransition(DataTransition dataTransition);

    boolean equals(State state);

    Set<WordTransition> getEnabledWordTransitions(String word, int value, Register register);

    Set<DataTransition> getEnabledDataTransitions(String word, int value, Register register);
}

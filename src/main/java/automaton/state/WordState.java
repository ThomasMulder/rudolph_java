package automaton.state;

import automaton.register.Register;
import automaton.transition.DataTransition;
import automaton.transition.WordTransition;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class WordState implements State {
    private String label;
    private Set<DataTransition> incomingTransitions;
    private Set<WordTransition> outgoingTransitions;
    private StateStatus status;

    public WordState(String label) {
        this.status = StateStatus.UNVISITED;
        initialise(label);
    }

    public WordState(String label, Set<DataTransition> incomingTransitions, Set<WordTransition> outgoingTransitions) {
        this.status = StateStatus.UNVISITED;
        initialise(label);
        this.incomingTransitions.addAll(incomingTransitions);
        this.outgoingTransitions.addAll(outgoingTransitions);
    }

    @Override
    public Set<WordTransition> getWordTransitions() {
        return outgoingTransitions;
    }

    @Override
    public Set<DataTransition> getDataTransitions() {
        return incomingTransitions;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public State copy() {
        return new WordState(label, incomingTransitions, outgoingTransitions);
    }

    @Override
    public StateStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(StateStatus status) {
        this.status = status;
    }

    @Override
    public void addWordTransition(WordTransition wordTransition) {
        outgoingTransitions.add(wordTransition);
    }

    @Override
    public void addDataTransition(DataTransition dataTransition) {
        incomingTransitions.add(dataTransition);
    }

    @Override
    public void removeWordTransition(WordTransition wordTransition) {
        outgoingTransitions.remove(wordTransition);
    }

    @Override
    public void removeDataTransition(DataTransition dataTransition) {
        incomingTransitions.remove(dataTransition);
    }

    @Override
    public boolean equals(State state) {
        if (this == state) return true;
        if (this.label == null && state.getLabel() == null) return true;
        if (this.label == null && state.getLabel() != null) return false;
        if (this.label != null && state.getLabel() == null) return false;
        return this.label.equals(state.getLabel());
    }

    @Override
    public Set<WordTransition> getEnabledWordTransitions(String word, int value, Register register) {
        Set<WordTransition> result = getWordTransitions().stream().filter(wordTransition -> wordTransition.isEnabled(word, value, register)).collect(Collectors.toSet());
        return result;
    }

    @Override
    public Set<DataTransition> getEnabledDataTransitions(String word, int value, Register register) {
        Set<DataTransition> result = getDataTransitions().stream().filter(dataTransition -> dataTransition.isEnabled(word, value, register)).collect(Collectors.toSet());
        return result;
    }

    private void initialise(String label) {
        this.label = label;
        this.incomingTransitions = new HashSet<DataTransition>();
        this.outgoingTransitions = new HashSet<WordTransition>();
    }

    public boolean hasEquivalentDataTransition(DataTransition d1) {
        for (DataTransition d : incomingTransitions) {
            if (d.isEquivalent(d1)) return true;
        }
        return false;
    }

    public boolean hasEquivalentWordTransition(WordTransition w1) {
        for (WordTransition w : outgoingTransitions) {
            if (w.isEquivalent(w1)) return true;
        }
        return false;
    }
}

package automaton.state;

public enum StateStatus {
    UNVISITED,
    VISITING,
    VISITED
}

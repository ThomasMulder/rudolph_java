package automaton.state;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class StateSet {
    private Set<State> states;

    public StateSet() {
        this.states = new HashSet<State>();
    }

    public void addState(State state) {
        if (!deepContains(state)) {
            states.add(state);
        }
    }

    public void addAll(StateSet stateSet) {
        for (State s : stateSet.getStates()) {
            addState(s);
        }
    }

    public void removeState(State state) {
        states.remove(state);
    }

    public Set<State> getStates() {
        return states;
    }

    public boolean contains(State state) {
        return deepContains(state);

    }

    public StateSet minimize(Set<State> exceptions) {
        StateSet stateSet = new StateSet();
        for (State state : states) {
            if (exceptions.contains(state)
                    || (state.getDataTransitions().size() > 0 && state.getWordTransitions().size() > 0)) {
                stateSet.addState(state);
            }
        }
        return stateSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StateSet stateSet = (StateSet) o;
        for (State s : states) {
            boolean contained = false;
            for (State t : stateSet.states) {
                if (s.equals(t)) {
                    contained = true;
                    break;
                }
            }
            if (!contained) return false;
        }
        return true;
    }

    private boolean deepContains(State state) {
        if (states.contains(state)) return true;
        for (State s : states) {
            if (s.getLabel() == null && state.getLabel() == null) return true;
            if (s.getLabel().equals(state.getLabel())) return true;
        }
        return false;
    }
}

package automaton.state;

import automaton.register.Register;
import automaton.transition.DataTransition;
import automaton.transition.Transition;
import automaton.transition.WordTransition;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class DataState implements State {
    private String label;
    private Set<WordTransition> incomingTransitions;
    private Set<DataTransition> outgoingTransitions;
    private StateStatus status;

    public DataState(String label) {
        this.status = StateStatus.UNVISITED;
        initialise(label);
    }

    public DataState(String label, Set<WordTransition> incomingTransitions, Set<DataTransition> outgoingTransitions) {
        this.status = StateStatus.UNVISITED;
        initialise(label);
        this.incomingTransitions.addAll(incomingTransitions);
        this.outgoingTransitions.addAll(outgoingTransitions);
    }

    @Override
    public Set<WordTransition> getWordTransitions() {
        return incomingTransitions;
    }

    @Override
    public Set<DataTransition> getDataTransitions() {
        return outgoingTransitions;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public State copy() {
        return new DataState(label, incomingTransitions, outgoingTransitions);
    }

    @Override
    public StateStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(StateStatus status) {
        this.status = status;
    }

    @Override
    public void addWordTransition(WordTransition wordTransition) {
        incomingTransitions.add(wordTransition);
    }

    @Override
    public void addDataTransition(DataTransition dataTransition) {
        outgoingTransitions.add(dataTransition);
    }

    @Override
    public void removeWordTransition(WordTransition wordTransition) {
        incomingTransitions.remove(wordTransition);
    }

    @Override
    public void removeDataTransition(DataTransition dataTransition) {
        outgoingTransitions.remove(dataTransition);
    }

    @Override
    public boolean equals(State state) {
        if (this == state) return true;
        if (this.label == null && state.getLabel() == null) return true;
        if (this.label == null && state.getLabel() != null) return false;
        if (this.label != null && state.getLabel() == null) return false;
        return this.label.equals(state.getLabel());
    }

    @Override
    public Set<WordTransition> getEnabledWordTransitions(String word, int value, Register register) {
        Set<WordTransition> result = getWordTransitions().stream().filter(wordTransition -> wordTransition.isEnabled(word, value, register)).collect(Collectors.toSet());
        return result;
    }

    @Override
    public Set<DataTransition> getEnabledDataTransitions(String word, int value, Register register) {
        Set<DataTransition> result = getDataTransitions().stream().filter(dataTransition -> dataTransition.isEnabled(word, value, register)).collect(Collectors.toSet());
        return result;
    }

    private void initialise(String label) {
        this.label = label;
        this.incomingTransitions = new HashSet<WordTransition>();
        this.outgoingTransitions = new HashSet<DataTransition>();
    }
}

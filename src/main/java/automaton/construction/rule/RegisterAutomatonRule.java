package automaton.construction.rule;

import automaton.RegisterAutomaton;
import automaton.condition.Condition;
import automaton.register.RegisterAssignment;

public interface RegisterAutomatonRule {

    RegisterAutomaton apply(RegisterAutomaton[] childAutomata, String word, int data, int k,
                            RegisterAssignment registerAssignment, Condition condition);
}

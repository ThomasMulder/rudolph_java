package automaton.construction.rule.expand;

import automaton.RegisterAutomaton;
import automaton.condition.AndCondition;
import automaton.condition.Condition;
import automaton.condition.construction.rule.merge.AndRule;
import automaton.register.RegisterAssignment;
import automaton.state.DataState;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.Transition;
import automaton.transition.TransitionSet;

public class RepeatExpansionRule extends RegisterAutomatonExpansionRule {

    @Override
    public RegisterAutomaton expand(RegisterAutomaton base, int k, RegisterAssignment registerAssignment, Condition condition) {
        TransitionSet newTransitions = new TransitionSet();
        for (Transition t : base.getDataTransitions().getTransitions()) {
            DataTransition dt = (DataTransition) t;
            if (base.getFinalStates().contains(dt.getTarget())) {
                for (Transition s : base.getDataTransitions().getTransitions()) {
                    DataTransition ds = (DataTransition) s;
                    if (base.getInitialState().equals(ds.getSource())) {
                        RegisterAssignment newRegisterAssignment = dt.getRegisterAssignment();
                        newRegisterAssignment.union(registerAssignment);
                        DataTransition newTransition = new DataTransition((DataState) dt.getSource(),
                                (WordState) ds.getTarget(), new AndRule().merge(dt.getCondition(), ds.getCondition()),
                                newRegisterAssignment);
                        newTransitions.addTransition(newTransition);
                    }
                }
            }
        }
        base.getDataTransitions().addAll(newTransitions);
        return base;
    }
}

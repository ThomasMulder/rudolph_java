package automaton.construction.rule.expand;

import automaton.RegisterAutomaton;
import automaton.condition.Condition;
import automaton.construction.rule.RegisterAutomatonRule;
import automaton.register.RegisterAssignment;

public abstract class RegisterAutomatonExpansionRule implements RegisterAutomatonRule {

    public RegisterAutomaton apply(RegisterAutomaton[] childAutomata, String word, int data, int k,
                                   RegisterAssignment registerAssignment, Condition condition) {
        if (childAutomata.length != 1) {
            throw new IllegalArgumentException("Expecting one child automaton here.");
        }
        return expand(childAutomata[0], k, registerAssignment, condition);
    }

    abstract public RegisterAutomaton expand(RegisterAutomaton base, int k, RegisterAssignment registerAssignment, Condition condition);
}

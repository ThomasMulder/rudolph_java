package automaton.construction.rule.expand;

import automaton.RegisterAutomaton;
import automaton.condition.AndCondition;
import automaton.condition.Condition;
import automaton.condition.EmptyCondition;
import automaton.condition.construction.rule.merge.AndRule;
import automaton.construction.StateNameGenerator;
import automaton.register.RegisterAssignment;
import automaton.state.DataState;
import automaton.state.StateSet;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.Transition;
import automaton.transition.TransitionSet;

public class IfExpansionRule extends RegisterAutomatonExpansionRule {

    @Override
    public RegisterAutomaton expand(RegisterAutomaton base, int k, RegisterAssignment registerAssignment,
                                    Condition condition) {
        StateNameGenerator stateNameGenerator = StateNameGenerator.getInstance();
        WordState wordState = new WordState(stateNameGenerator.getWordStateName());
        base.addWordState(wordState);
        TransitionSet newTransitions = new TransitionSet();
        for (Transition transition : base.getDataTransitions().getTransitions()) {
            DataTransition dataTransition = (DataTransition) transition;
            if (base.getFinalStates().contains(dataTransition.getTarget())) {
                Condition newCondition = null;
                if (dataTransition.getCondition() == null || dataTransition.getCondition() instanceof EmptyCondition) {
                    newCondition = condition;
                } else {
                    newCondition = new AndRule().merge(dataTransition.getCondition(), condition);
                }
                DataTransition newTransition = new DataTransition((DataState) dataTransition.getSource(), wordState,
                        newCondition, registerAssignment);
                newTransitions.addTransition(newTransition);
            }
        }
        base.getDataTransitions().addAll(newTransitions);
        StateSet finalStates = new StateSet();
        finalStates.addState(wordState);
        base.setFinalStates(finalStates);
        return base;
    }
}

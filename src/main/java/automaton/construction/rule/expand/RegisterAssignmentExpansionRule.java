package automaton.construction.rule.expand;

import automaton.RegisterAutomaton;
import automaton.condition.Condition;
import automaton.construction.StateNameGenerator;
import automaton.register.RegisterAssignment;
import automaton.state.DataState;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.Transition;
import automaton.transition.TransitionSet;

public class RegisterAssignmentExpansionRule extends RegisterAutomatonExpansionRule {

    @Override
    public RegisterAutomaton expand(RegisterAutomaton base, int k, RegisterAssignment registerAssignment, Condition condition) {
        StateNameGenerator stateNameGenerator = StateNameGenerator.getInstance();
        DataState dataState = new DataState(stateNameGenerator.getDataStateName());
        base.addDataState(dataState);
        TransitionSet newTransitions = new TransitionSet();
        for (Transition transition : base.getDataTransitions().getTransitions()) {
            DataTransition dataTransition = (DataTransition) transition;
            if (dataTransition.getSource().equals(base.getInitialState())) {
                RegisterAssignment newRegisterAssignment = dataTransition.getRegisterAssignment();
                newRegisterAssignment.union(registerAssignment);
                DataTransition newTransition = new DataTransition(dataState, (WordState) dataTransition.getTarget(),
                        dataTransition.getCondition(), newRegisterAssignment);
                newTransitions.addTransition(newTransition);
            }
        }
        base.setInitialState(dataState);
        base.getDataTransitions().addAll(newTransitions);
        return base;
    }
}

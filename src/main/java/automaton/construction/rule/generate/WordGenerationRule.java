package automaton.construction.rule.generate;

import automaton.RegisterAutomaton;
import automaton.condition.EmptyCondition;
import automaton.construction.StateNameGenerator;
import automaton.register.Register;
import automaton.register.RegisterAssignment;
import automaton.state.DataState;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.WordTransition;

public class WordGenerationRule extends RegisterAutomatonGenerationRule {

    @Override
    public RegisterAutomaton generate(String word, int k) {
        StateNameGenerator stateNameGenerator = StateNameGenerator.getInstance();
        RegisterAutomaton registerAutomaton = new RegisterAutomaton(k);
        DataState d1 = new DataState(stateNameGenerator.getDataStateName());
        DataState d2 = new DataState(stateNameGenerator.getDataStateName());
        WordState w1 = new WordState(stateNameGenerator.getWordStateName());
        WordState w2 = new WordState(stateNameGenerator.getWordStateName());
        registerAutomaton.addWordState(w1);
        registerAutomaton.addWordState(w1);
        registerAutomaton.addWordState(w2);
        registerAutomaton.addDataState(d1);
        registerAutomaton.addDataState(d2);
        registerAutomaton.setInitialState(d1);
        registerAutomaton.addFinalState(w2);
        WordTransition wt1 = new WordTransition(w1, d2, word);
        DataTransition dt1 = new DataTransition(d1, w1, new EmptyCondition(), new RegisterAssignment());
        DataTransition dt2 = new DataTransition(d2, w2, new EmptyCondition(), new RegisterAssignment());
        registerAutomaton.addWordTransition(wt1);
        registerAutomaton.addDataTransition(dt1);
        registerAutomaton.addDataTransition(dt2);
        return registerAutomaton;
    }
}

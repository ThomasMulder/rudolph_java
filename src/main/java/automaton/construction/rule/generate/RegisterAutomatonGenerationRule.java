package automaton.construction.rule.generate;

import automaton.RegisterAutomaton;
import automaton.condition.Condition;
import automaton.construction.rule.RegisterAutomatonRule;
import automaton.register.RegisterAssignment;

public abstract class RegisterAutomatonGenerationRule implements RegisterAutomatonRule {

    public abstract RegisterAutomaton generate(String word, int k);

    public RegisterAutomaton apply(RegisterAutomaton[] childAutomata, String word, int data, int k,
                                   RegisterAssignment registerAssignment, Condition condition) {
        return generate(word, k);
    }
}

package automaton.construction.rule.generate;

import automaton.RegisterAutomaton;
import automaton.condition.EmptyCondition;
import automaton.construction.StateNameGenerator;
import automaton.register.Register;
import automaton.register.RegisterAssignment;
import automaton.state.DataState;
import automaton.state.WordState;
import automaton.transition.DataTransition;

public class NullGenerationRule extends RegisterAutomatonGenerationRule {

    @Override
    public RegisterAutomaton generate(String word, int k) {
        StateNameGenerator stateNameGenerator = StateNameGenerator.getInstance();
        RegisterAutomaton registerAutomaton = new RegisterAutomaton(k);
        DataState d = new DataState(stateNameGenerator.getDataStateName());
        WordState w = new WordState(stateNameGenerator.getWordStateName());
        registerAutomaton.setInitialState(d);
        registerAutomaton.addDataState(d);
        registerAutomaton.addWordState(w);
        registerAutomaton.addFinalState(w);
        DataTransition dt = new DataTransition(d, w, new EmptyCondition(), new RegisterAssignment());
        registerAutomaton.addDataTransition(dt);
        return registerAutomaton;
    }
}

package automaton.construction.rule.merge;

import automaton.RegisterAutomaton;
import automaton.construction.StateNameGenerator;
import automaton.register.Register;
import automaton.register.RegisterAssignment;
import automaton.state.DataState;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.Transition;
import automaton.transition.TransitionSet;

public class OrMergeRule extends RegisterAutomatonMergeRule {

    @Override
    public RegisterAutomaton merge(RegisterAutomaton lhs, RegisterAutomaton rhs, int k) {
        StateNameGenerator stateNameGenerator = StateNameGenerator.getInstance();
        DataState dataState = new DataState(stateNameGenerator.getDataStateName());
        lhs.getDataStates().addState(dataState);
        lhs.getDataStates().addAll(rhs.getDataStates());
        lhs.getWordStates().addAll(rhs.getWordStates());
        lhs.getFinalStates().addAll(rhs.getFinalStates());
        lhs.getWordTransitions().addAll(rhs.getWordTransitions());
        lhs.getDataTransitions().addAll(rhs.getDataTransitions());
        TransitionSet newTransitions = new TransitionSet();
        for (Transition transition : lhs.getDataTransitions().getTransitions()) {
            DataTransition dataTransition = (DataTransition) transition;
            if (dataTransition.getSource().equals(lhs.getInitialState())
                    || dataTransition.getSource().equals(rhs.getInitialState())) {
                newTransitions.addTransition(new DataTransition(dataState,
                        (WordState) dataTransition.getTarget(), dataTransition.getCondition(), new RegisterAssignment()));
            }
        }
        lhs.getDataTransitions().addAll(newTransitions);
        lhs.setInitialState(dataState);
        return lhs;
    }
}

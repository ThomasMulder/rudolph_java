package automaton.construction.rule.merge;

import automaton.RegisterAutomaton;
import automaton.condition.Condition;
import automaton.construction.rule.RegisterAutomatonRule;
import automaton.register.RegisterAssignment;

public abstract class RegisterAutomatonMergeRule implements RegisterAutomatonRule {

    public RegisterAutomaton apply(RegisterAutomaton[] childAutomata, String word, int data, int k,
                                   RegisterAssignment registerAssignment, Condition condition) {
        if (childAutomata.length != 2) {
            throw new IllegalArgumentException("Expecting two child automata here.");
        }
        return merge(childAutomata[0], childAutomata[1], k);
    }

    public abstract RegisterAutomaton merge(RegisterAutomaton lhs, RegisterAutomaton rhs, int k);
}

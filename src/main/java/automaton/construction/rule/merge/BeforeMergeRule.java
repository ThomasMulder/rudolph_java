package automaton.construction.rule.merge;

import automaton.RegisterAutomaton;
import automaton.condition.AndCondition;
import automaton.condition.construction.rule.merge.AndRule;
import automaton.register.RegisterAssignment;
import automaton.state.DataState;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.Transition;
import automaton.transition.TransitionSet;

public class BeforeMergeRule extends RegisterAutomatonMergeRule {

    @Override
    public RegisterAutomaton merge(RegisterAutomaton lhs, RegisterAutomaton rhs, int k) {
        lhs.getDataStates().addAll(rhs.getDataStates());
        lhs.getWordStates().addAll(rhs.getWordStates());
        TransitionSet newDataTransitions = new TransitionSet();
        for (Transition t1 : lhs.getDataTransitions().getTransitions()) {
            for (Transition t2 : rhs.getDataTransitions().getTransitions()) {
                DataTransition d1 = (DataTransition) t1;
                DataTransition d2 = (DataTransition) t2;
                if (lhs.getFinalStates().contains(d1.getTarget())
                        && rhs.getInitialState().equals(d2.getSource())) {
                    RegisterAssignment newRegisterAssignment = d1.getRegisterAssignment();
                    newRegisterAssignment.union(d2.getRegisterAssignment());
                    DataTransition newTransition = new DataTransition((DataState) d1.getSource(),
                            (WordState) d2.getTarget(), new AndRule().merge(d1.getCondition(), d2.getCondition()),
                            newRegisterAssignment);
                    newDataTransitions.addTransition(newTransition);
                }
            }
        }
        lhs.getDataTransitions().addAll(rhs.getDataTransitions());
        lhs.getDataTransitions().addAll(newDataTransitions);
        lhs.getWordTransitions().addAll(rhs.getWordTransitions());
        lhs.setFinalStates(rhs.getFinalStates());
        return lhs;
    }
}

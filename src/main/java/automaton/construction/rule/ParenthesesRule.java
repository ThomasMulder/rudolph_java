package automaton.construction.rule;

import automaton.RegisterAutomaton;
import automaton.condition.Condition;
import automaton.register.RegisterAssignment;

public class ParenthesesRule implements RegisterAutomatonRule {

    @Override
    public RegisterAutomaton apply(RegisterAutomaton[] childAutomata, String word, int data, int k, RegisterAssignment registerAssignment, Condition condition) {
        if (childAutomata.length != 1) {
            throw new IllegalArgumentException("Expecting one child automaton here.");
        }
        return childAutomata[0];
    }
}

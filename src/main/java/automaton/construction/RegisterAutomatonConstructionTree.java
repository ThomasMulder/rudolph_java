package automaton.construction;

import REM.REMParser;
import automaton.condition.Condition;
import automaton.condition.EmptyCondition;
import automaton.condition.construction.ConditionConstructionTree;
import automaton.construction.rule.ParenthesesRule;
import automaton.construction.rule.expand.IfExpansionRule;
import automaton.construction.rule.expand.RegisterAssignmentExpansionRule;
import automaton.register.RegisterAssignment;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;
import automaton.RegisterAutomaton;
import automaton.construction.rule.RegisterAutomatonRule;
import automaton.construction.rule.expand.RepeatExpansionRule;
import automaton.construction.rule.generate.WordGenerationRule;
import automaton.construction.rule.merge.BeforeMergeRule;
import automaton.construction.rule.merge.OrMergeRule;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class RegisterAutomatonConstructionTree {
    private RegisterAutomatonConstructionTree parent;
    private ParseTree parseTree;
    private RegisterAutomaton registerAutomaton;
    private RegisterAutomatonRule rule;
    private String word;
    private int data;
    private Stack<Integer> registerAssignment;
    private Condition condition;

    public RegisterAutomatonConstructionTree(RegisterAutomatonConstructionTree parent, ParseTree parseTree) {
        this.parent = parent;
        this.parseTree = parseTree;
        this.registerAssignment = new Stack<Integer>();
        this.condition = new EmptyCondition();
    }

    public void construct(int k) {
        if (parseTree instanceof REMParser.AssignmentContext) {
            constructRegisterAssignment(registerAssignment, parseTree);
            parent.registerAssignment = registerAssignment;
        }
        else if (parseTree instanceof REMParser.ConditionContext) {
            ConditionConstructionTree conditionConstructionTree = new ConditionConstructionTree(null, parseTree);
            conditionConstructionTree.construct();
            parent.condition = conditionConstructionTree.getCondition();
        } else {
            List<RegisterAutomaton> childAutomata = new ArrayList<RegisterAutomaton>();
            for (int i = 0; i < parseTree.getChildCount(); i++) {
                RegisterAutomatonConstructionTree child = new RegisterAutomatonConstructionTree(this, parseTree.getChild(i));
                child.construct(k);
                RegisterAutomaton childAutomaton = child.getRegisterAutomaton();
                if (childAutomaton != null) {
                    childAutomata.add(childAutomaton);
                }
            }
            RegisterAutomaton[] nonEmptyChildAutomata = removeEmptyAutomata(childAutomata);
            if (rule != null) {
                registerAutomaton = rule.apply(nonEmptyChildAutomata, word, data, k, constructRegisterAssignmentFromStack(), condition);
            }
            if (parseTree instanceof TerminalNodeImpl && parent != null) {
                TerminalNodeImpl node = (TerminalNodeImpl) parseTree;
                int type = node.getSymbol().getType();
                switch (type) {
                    case REMParser.LPARENTHESIS: // Expression Parentheses
                        if (this.parent.rule == null) this.parent.setRule(new ParenthesesRule());
                        break;
                    case REMParser.OR: // Expression OR
                        this.parent.setRule(new OrMergeRule());
                        break;
                    case REMParser.BEFORE: // Expression BEFORE
                        this.parent.setRule(new BeforeMergeRule());
                        break;
                    case REMParser.REPEAT: // Expression REPEAT
                        this.parent.setRule(new RepeatExpansionRule());
                        break;
                    case REMParser.IF: // Expression IF
                        this.parent.setRule(new IfExpansionRule());
                        break;
                    case REMParser.REGISTER: // Expression REGISTER
                        this.parent.setRule(new RegisterAssignmentExpansionRule());
                        break;
                    case REMParser.NULL: // Data NULL
                        this.parent.setData(-1);
                        this.parent.setRule(new WordGenerationRule());
                        break;
                    case REMParser.LETTER: // Data WORD
                        this.parent.setRegisterAutomaton(new WordGenerationRule().generate(node.getText(), k));
                        break;
                }
            }
        }
    }

    private void constructRegisterAssignment(Stack<Integer> assignment, ParseTree parseTree) {
        assignment.push(Integer.valueOf(parseTree.getChild(0).getText()));
        if (parseTree.getChildCount() > 1) {
            constructRegisterAssignment(assignment, parseTree.getChild(2));
        }
    }

    public void setRule(RegisterAutomatonRule rule) {
        this.rule = rule;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setWord(String word) { this.word = word; }

    public RegisterAutomaton getRegisterAutomaton() {
        return this.registerAutomaton;
    }

    public void setRegisterAutomaton(RegisterAutomaton registerAutomaton) {
        this.registerAutomaton = registerAutomaton;
    }

    public RegisterAssignment constructRegisterAssignmentFromStack() {
        RegisterAssignment result = new RegisterAssignment();
        while (!registerAssignment.isEmpty()) {
            int value = registerAssignment.pop();
            if (value > -1) result.getAssignment().add(value);
        }
        return result;
    }

    private RegisterAutomaton[] removeEmptyAutomata(List<RegisterAutomaton> automata) {
        List<RegisterAutomaton> aux  = new ArrayList<RegisterAutomaton>();
        for (RegisterAutomaton registerAutomaton :  automata) {
            if (registerAutomaton != null) {
                aux.add(registerAutomaton);
            }
        }
        RegisterAutomaton[] result = new RegisterAutomaton[aux.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = aux.get(i);
        }
        return result;
    }
}

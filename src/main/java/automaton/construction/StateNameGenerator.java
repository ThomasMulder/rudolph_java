package automaton.construction;

public class StateNameGenerator {
    private static StateNameGenerator instance;
    private int wordStateCount, dataStateCount;

    private StateNameGenerator() {
        this.wordStateCount = 0;
        this.dataStateCount = 0;
    }

    public static StateNameGenerator getInstance() {
        if (instance == null) {
            instance = new StateNameGenerator();
        }
        return instance;
    }

    public String getWordStateName() {
        this.wordStateCount++;
        return "w" + String.valueOf(this.wordStateCount);
    }

    public String getDataStateName() {
        this.dataStateCount++;
        return "d" + String.valueOf(this.dataStateCount);
    }

    public void reset() {
        wordStateCount = 0;
        dataStateCount = 0;
    }
}

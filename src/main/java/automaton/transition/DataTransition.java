package automaton.transition;

import automaton.condition.Condition;
import automaton.register.Register;
import automaton.register.RegisterAssignment;
import automaton.state.DataState;
import automaton.state.State;
import automaton.state.WordState;

import java.util.Objects;

public class DataTransition implements Transition {
    private DataState source;
    private WordState target;
    private Condition condition;
    private RegisterAssignment registerAssignment;
    private boolean locked;

    public DataTransition(DataState source, WordState target, Condition condition, RegisterAssignment registerAssignment) {
        this.source = source;
        this.target = target;
        this.condition = condition;
        this.registerAssignment = registerAssignment;
        this.locked = false;
        updateStates();
    }

    @Override
    public State getSource() {
        return source;
    }

    @Override
    public State getTarget() {
        return target;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setRegisterAssignment(RegisterAssignment registerAssignment) {
        this.registerAssignment = registerAssignment;
    }

    public RegisterAssignment getRegisterAssignment() {
        return registerAssignment;
    }

    @Override
    public boolean isEnabled(String word, int value, Register register) {
        return condition.isTrue(value, register) && !locked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataTransition that = (DataTransition) o;
        boolean a = source.equals(that.source);
        boolean b = target.equals(that.target);
        boolean c = registerAssignment.equals(that.registerAssignment);
        boolean d = condition.isEquivalent(that.condition);
        return source.equals(that.source) &&
                target.equals(that.target) &&
                condition.isEquivalent(that.condition) &&
                registerAssignment.equals(that.registerAssignment);
    }

    public boolean isEquivalent(DataTransition dataTransition) {
        if (this == dataTransition) return true;
        return source.equals(dataTransition.getSource()) && condition.isEquivalent(dataTransition.getCondition())
                && registerAssignment.equals(dataTransition.getRegisterAssignment());
    }

    public void lock() {
        locked = true;
    }

    public void unlock() {
        locked = false;
    }

    public String getTransitionLabel() {
        return "{" + condition.toString() + "," + registerAssignment.toString() + "}";
    }

    private void updateStates() {
        source.addDataTransition(this);
        target.addDataTransition(this);
    }
}

package automaton.transition;

import automaton.register.Register;
import automaton.state.DataState;
import automaton.state.State;
import automaton.state.WordState;

import java.util.Objects;

public class WordTransition implements Transition {
    private WordState source;
    private DataState target;
    private String word;

    public WordTransition(WordState source, DataState target, String word) {
        this.source = source;
        this.target = target;
        this.word = word;
        updateStates();
    }

    @Override
    public State getSource() {
        return source;
    }

    @Override
    public State getTarget() {
        return target;
    }

    public String getWord() {
        return word;
    }

    @Override
    public boolean isEnabled(String word, int value, Register register) {
        if (this.word == null || this.word.equals("null")) return true;
        return this.word.equals(word);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WordTransition that = (WordTransition) o;
        if (word == null || that.word == null) {
            return source.equals(that.source) && target.equals(that.target) && null == word && null == that.word;
        }
        return source.equals(that.source) &&
                target.equals(that.target) &&
                word.equals(that.word);
    }

    public void lockReverseTransitions() {
        for (DataTransition dataTransition : target.getDataTransitions()) {
            if (dataTransition.getTarget().equals(source)) dataTransition.lock();
        }
    }

    public void unlockReverseTransitions() {
        for (DataTransition dataTransition : target.getDataTransitions()) {
            if (dataTransition.getTarget().equals(source)) dataTransition.unlock();
        }
    }

    private void updateStates() {
        source.addWordTransition(this);
        target.addWordTransition(this);
    }

    public boolean isEquivalent(WordTransition w1) {
        if (word == null && w1.getWord() == null) return true;
        if (word == null && w1.getWord() != null) return false;
        if (word != null && w1.getWord() == null) return false;
        if (this == w1) return true;
        return target.equals(w1.getTarget()) && word.equals(w1.getWord());
    }
}

package automaton.transition;

import automaton.state.State;
import automaton.state.StateSet;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class TransitionSet {
    private Set<Transition> transitions;

    public TransitionSet() {
        this.transitions = new HashSet<Transition>();
    }

    public void addTransition(Transition transition) {
        if (!deepContains(transition)) {
            transitions.add(transition);
        }
    }

    public Set<Transition> getTransitions() {
        return transitions;
    }

    public void addAll(TransitionSet transitionSet) {
        for (Transition transition : transitionSet.transitions) {
            addTransition(transition);

        }
    }

    public void removeTransition(Transition transition) {
        transitions.remove(transition);
    }

    public TransitionSet minimize(StateSet wordStates, StateSet dataStates) {
        TransitionSet transitionSet = new TransitionSet();
        for (Transition transition : transitions) {
            State source = transition.getSource();
            State target = transition.getTarget();
            if ((wordStates.contains(source) && dataStates.contains(target))
                    || (dataStates.contains(source) && wordStates.contains(target))) {
                transitionSet.addTransition(transition);
            } else {
                if (transition instanceof DataTransition) {
                    source.removeDataTransition((DataTransition) transition);
                    target.removeDataTransition((DataTransition) transition);
                } else {
                    source.removeWordTransition((WordTransition) transition);
                    target.removeWordTransition((WordTransition) transition);
                }
            }
        }
        return transitionSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransitionSet that = (TransitionSet) o;
        for (Transition t : transitions) {
            boolean contained = false;
            for (Transition s : that.transitions) {
                if (t.equals(s)) {
                    contained = true;
                    break;
                }
            }
            if (!contained) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {

        return Objects.hash(transitions);
    }

    private boolean deepContains(Transition transition) {
        if (transitions.contains(transition)) return true;
        for (Transition t : transitions) {
            if (t.getSource().equals(transition.getSource()) && t.getTarget().equals(transition.getTarget())) {
                return true;
            }
        }
        return false;
    }
}

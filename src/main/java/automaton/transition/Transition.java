package automaton.transition;

import automaton.register.Register;
import automaton.state.State;

public interface Transition {

    State getSource();

    State getTarget();

    boolean isEnabled(String word, int value, Register register);
}

package automaton.condition;

import automaton.register.Register;

public interface Condition {

    boolean isTrue(int value, Register register);

    boolean equals(Condition condition);

    boolean isEquivalent(Condition condition);
}

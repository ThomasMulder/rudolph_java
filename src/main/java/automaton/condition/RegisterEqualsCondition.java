package automaton.condition;

import automaton.register.Register;

public class RegisterEqualsCondition implements Condition {
    private int index;

    public RegisterEqualsCondition(int index) {
        this.index = index;
    }

    public boolean isTrue(int value, Register register) {
        return register.getRegister()[index] == value;
    }

    public boolean equals(Condition condition) {
        return false;
    }

    @Override
    public boolean isEquivalent(Condition condition) {
        if (this == condition) return true;
        if (!(condition instanceof RegisterEqualsCondition)) return false;
        RegisterEqualsCondition a = (RegisterEqualsCondition) condition;

        return index == a.index;
    }
}

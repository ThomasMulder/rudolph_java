package automaton.condition;

import automaton.register.Register;

public class AndCondition implements Condition {
    private Condition leftCondition, rightCondition;

    public AndCondition(Condition leftCondition, Condition rightCondition) {
        this.leftCondition = leftCondition;
        this.rightCondition = rightCondition;
    }

    public boolean isTrue(int value, Register register) {
        return leftCondition.isTrue(value, register) && rightCondition.isTrue(value, register);
    }

    public boolean equals(Condition condition) {
        if (!(condition instanceof AndCondition)) return false;
        AndCondition c = (AndCondition) condition;

        return leftCondition.equals(c.leftCondition) && rightCondition.equals(c.rightCondition);
    }

    @Override
    public boolean isEquivalent(Condition condition) {
        if (this == condition) return true;
        if (!(condition instanceof AndCondition)) return false;
        AndCondition a = (AndCondition) condition;

        return leftCondition.isEquivalent(a.leftCondition) && rightCondition.isEquivalent(rightCondition);
    }
}

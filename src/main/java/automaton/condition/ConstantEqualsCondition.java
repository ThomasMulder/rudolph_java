package automaton.condition;

import automaton.register.Register;

public class ConstantEqualsCondition implements Condition {
    private int constant;

    public ConstantEqualsCondition(int constant) {
        this.constant = constant;
    }

    public boolean isTrue(int value, Register register) {
        return constant == value;
    }

    public boolean equals(Condition condition) {
        return false;
    }

    @Override
    public boolean isEquivalent(Condition condition) {
        if (this == condition) return true;
        if (!(condition instanceof ConstantEqualsCondition)) return false;
        ConstantEqualsCondition a = (ConstantEqualsCondition) condition;

        return constant == a.constant;
    }
}

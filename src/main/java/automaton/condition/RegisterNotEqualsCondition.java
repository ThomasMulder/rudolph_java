package automaton.condition;

import automaton.register.Register;

public class RegisterNotEqualsCondition implements Condition {
    private int index;

    public RegisterNotEqualsCondition(int index) {
        this.index = index;
    }

    public boolean isTrue(int value, Register register) {
        return register.getRegister()[index] != value;
    }

    public boolean equals(Condition condition) {
        return false;
    }

    @Override
    public boolean isEquivalent(Condition condition) {
        if (this == condition) return true;
        if (!(condition instanceof RegisterNotEqualsCondition)) return false;
        RegisterNotEqualsCondition a = (RegisterNotEqualsCondition) condition;

        return index == a.index;
    }
}

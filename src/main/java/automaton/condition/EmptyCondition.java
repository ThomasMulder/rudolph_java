package automaton.condition;

import automaton.register.Register;

public class EmptyCondition implements Condition {

    @Override
    public boolean isTrue(int value, Register register) {
        return true;
    }

    @Override
    public boolean equals(Condition condition) {
        if (this == condition) return true;
        return (condition instanceof EmptyCondition);
    }

    @Override
    public boolean isEquivalent(Condition condition) {
        return condition instanceof EmptyCondition;
    }

    @Override
    public String toString() {
        return "\u03B5";
    }
}

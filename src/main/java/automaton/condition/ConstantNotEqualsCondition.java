package automaton.condition;

import automaton.register.Register;

public class ConstantNotEqualsCondition implements Condition{
    private int constant;

    public ConstantNotEqualsCondition(int constant) {
        this.constant = constant;
    }

    public boolean isTrue(int value, Register register) {
        return constant != value;
    }

    public boolean equals(Condition condition) {
        return false;
    }

    @Override
    public boolean isEquivalent(Condition condition) {
        if (this == condition) return true;
        if (!(condition instanceof ConstantNotEqualsCondition)) return false;
        ConstantNotEqualsCondition a = (ConstantNotEqualsCondition) condition;

        return constant == a.constant;
    }
}

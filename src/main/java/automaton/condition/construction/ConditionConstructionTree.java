package automaton.condition.construction;

import REM.REMParser;
import automaton.condition.*;
import automaton.condition.construction.rule.ConditionRule;
import automaton.condition.construction.rule.expand.NotRule;
import automaton.condition.construction.rule.merge.AndRule;
import automaton.condition.construction.rule.merge.OrRule;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import java.util.ArrayList;
import java.util.List;

public class ConditionConstructionTree {
    private ConditionConstructionTree parent;
    private ParseTree parseTree;

    private Condition condition;
    private ConditionRule rule;

    public ConditionConstructionTree(ConditionConstructionTree parent, ParseTree parseTree) {
        this.parent = parent;
        this.parseTree = parseTree;
    }

    public void construct() {
        List<Condition> childConditions = new ArrayList<Condition>();
        for (int i = 0; i< parseTree.getChildCount(); i++) {
            ConditionConstructionTree child = new ConditionConstructionTree(this, parseTree.getChild(i));
            child.construct();
            Condition childCondition = child.getCondition();
            if (childCondition != null) {
                childConditions.add(childCondition);
            }
        }
        Condition[] nonEmptyChildConditions = removeEmptyChildConditions(childConditions);
        if (rule != null) {
            condition = rule.apply(nonEmptyChildConditions);
        }
        if (parseTree instanceof TerminalNodeImpl && parent != null) {
            TerminalNodeImpl node = (TerminalNodeImpl) parseTree;
            ParseTree parseTreeSibling = parseTree.getParent().getChild(2);
            TerminalNodeImpl sibling = null;
            if (parseTreeSibling != null && !(parseTreeSibling instanceof REMParser.ConditionContext)) sibling = (TerminalNodeImpl) parseTreeSibling;
            int type = node.getSymbol().getType();
            switch(type) {
                case REMParser.CAND: // Condition AND
                    parent.setRule(new AndRule());
                    break;
                case REMParser.COR: // Condition OR
                    parent.setRule(new OrRule());
                    break;
                case REMParser.CNOT: // Condition NOT
                    parent.setRule(new NotRule());
                    break;
                case REMParser.REGEQ: // Condition REGEQ
                    parent.setCondition(new RegisterEqualsCondition(Integer.valueOf(sibling.getText())));
                    break;
                case REMParser.REGNEQ: // Condition REGNEQ
                    parent.setCondition(new RegisterNotEqualsCondition(Integer.valueOf(sibling.getText())));
                    break;
                case REMParser.EQ: // Condition EQ
                    parent.setCondition(new ConstantEqualsCondition(Integer.valueOf(sibling.getText())));
                    break;
                case REMParser.NEQ: // Condition NEQ
                    parent.setCondition(new ConstantNotEqualsCondition(Integer.valueOf(sibling.getText())));
                    break;
                case REMParser.NUMBER: // Data NUMBER
                    // TODO: Implement
                    break;
            }
        }
    }

    public void setRule(ConditionRule rule) {
        this.rule = rule;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public Condition getCondition() {
        return condition;
    }

    private Condition[] removeEmptyChildConditions(List<Condition> conditions) {
        List<Condition> aux = new ArrayList<Condition>();
        for (Condition condition : conditions) {
            if (condition != null) {
                aux.add(condition);
            }
        }
        Condition[] result = new Condition[aux.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = aux.get(i);
        }
        return result;
    }
}

package automaton.condition.construction.rule.merge;

import automaton.condition.Condition;
import automaton.condition.construction.rule.ConditionRule;

public abstract class ConditionMergeRule implements ConditionRule {

    @Override
    public Condition apply(Condition[] childConditions) {
        if (childConditions.length != 2) throw new IllegalArgumentException("Need two child condition here.");
        return merge(childConditions[0], childConditions[1]);
    }

    public abstract Condition merge(Condition lhs, Condition rhs);
}

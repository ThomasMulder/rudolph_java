package automaton.condition.construction.rule.merge;

import automaton.condition.AndCondition;
import automaton.condition.Condition;
import automaton.condition.EmptyCondition;

public class AndRule extends ConditionMergeRule {

    @Override
    public Condition merge(Condition lhs, Condition rhs) {
        if (lhs instanceof EmptyCondition) return rhs;
        if (rhs instanceof EmptyCondition) return lhs;
        return new AndCondition(lhs, rhs);
    }
}

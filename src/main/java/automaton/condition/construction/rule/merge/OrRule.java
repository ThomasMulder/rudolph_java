package automaton.condition.construction.rule.merge;

import automaton.condition.Condition;
import automaton.condition.EmptyCondition;
import automaton.condition.OrCondition;

public class OrRule extends ConditionMergeRule {

    @Override
    public Condition merge(Condition lhs, Condition rhs) {
        if (lhs instanceof EmptyCondition) return lhs;
        if (rhs instanceof EmptyCondition) return rhs;
        return new OrCondition(lhs, rhs);
    }
}

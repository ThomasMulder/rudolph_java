package automaton.condition.construction.rule;

import automaton.condition.Condition;

public interface ConditionRule {

    Condition apply(Condition[] childConditions);
}

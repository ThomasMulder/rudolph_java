package automaton.condition.construction.rule.expand;

import automaton.condition.Condition;
import automaton.condition.construction.rule.ConditionRule;

public abstract class ConditionExpansionRule implements ConditionRule {

    @Override
    public Condition apply(Condition[] childConditions) {
        if (childConditions.length != 1) throw new IllegalArgumentException("Need exactly one base condition here.");
        return expand(childConditions[0]);
    }

    abstract Condition expand(Condition base);
}

package automaton.condition.construction.rule.expand;

import automaton.condition.Condition;
import automaton.condition.EmptyCondition;
import automaton.condition.NotCondition;

public class NotRule extends ConditionExpansionRule  {

    @Override
    Condition expand(Condition base) {
        if (base instanceof EmptyCondition) return base;
        return new NotCondition(base);
    }
}

package automaton.condition;

import automaton.register.Register;

public class NotCondition implements Condition {
    private Condition condition;

    public NotCondition(Condition condition) {
        this.condition = condition;
    }

    public boolean isTrue(int value, Register register) {
        return !condition.isTrue(value, register);
    }

    public boolean equals(Condition condition) {
        if (!(condition instanceof NotCondition)) return false;
        NotCondition c = (NotCondition) condition;

        return condition.equals(c);
    }

    @Override
    public boolean isEquivalent(Condition condition) {
        if (this == condition) return true;
        if (!(condition instanceof NotCondition)) return false;
        NotCondition a = (NotCondition) condition;

        return this.condition.isEquivalent(a.condition);
    }
}

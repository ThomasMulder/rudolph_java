package automaton;

import REM.REMLexer;
import REM.REMParser;
import automaton.register.Register;
import automaton.state.*;
import automaton.transition.DataTransition;
import automaton.transition.Transition;
import automaton.transition.TransitionSet;
import automaton.transition.WordTransition;
import graph.Edge;
import graph.Graph;
import graph.Node;
import util.Pair;

import java.util.*;

public class RegisterAutomaton {
    private StateSet dataStates;
    private StateSet wordStates;
    private DataState initialState;
    private StateSet finalStates;
    private TransitionSet wordTransitions;
    private TransitionSet dataTransitions;
    private Register register;

    public RegisterAutomaton(int k) {
        initialise();
        this.register = new Register(k);
    }

    public RegisterAutomaton(int[] register) {
        initialise();
        this.register = new Register(register);
    }

    public void setInitialState(DataState initialState) {
        this.initialState = initialState;
    }

    public void setFinalStates(StateSet finalStates) {
        this.finalStates = finalStates;
    }

    public void setDataStates(StateSet dataStates) {
        this.dataStates = dataStates;
    }

    public void setWordStates(StateSet wordStates) {
        this.wordStates = wordStates;
    }

    public void setWordTransitions(TransitionSet wordTransitions) {
        this.wordTransitions = wordTransitions;
    }

    public void setDataTransitions(TransitionSet dataTransitions) {
        this.dataTransitions = dataTransitions;
    }

    public DataState getInitialState() {
        return initialState;
    }

    public Register getRegister() {
        return register;
    }

    public StateSet getDataStates() {
        return dataStates;
    }

    public StateSet getWordStates() {
        return wordStates;
    }

    public StateSet getFinalStates() {
        return finalStates;
    }

    public TransitionSet getWordTransitions() {
        return wordTransitions;
    }

    public TransitionSet getDataTransitions() {
        return dataTransitions;
    }

    public void addDataState(DataState dataState) {
        dataStates.addState(dataState);
    }

    public void addWordState(WordState wordState) {
        wordStates.addState(wordState);
    }

    public void addFinalState(WordState wordState) {
        finalStates.addState(wordState);
    }

    public void addDataTransition(DataTransition dataTransition) {
        dataTransitions.addTransition(dataTransition);
    }

    public void addWordTransition(WordTransition wordTransition) {
        wordTransitions.addTransition(wordTransition);
    }

    public void setAllStatusesUnvisited() {
        for (State state : dataStates.getStates()) {
            state.setStatus(StateStatus.UNVISITED);
        }
        for (State state : wordStates.getStates()) {
            state.setStatus(StateStatus.UNVISITED);
        }
    }

    public RegisterAutomaton minimize() {
        Set<State> exceptions = new HashSet<State>();
        exceptions.add(initialState);
        exceptions.addAll(finalStates.getStates());
        RegisterAutomaton registerAutomaton = new RegisterAutomaton(register.getRegister());
        registerAutomaton.setInitialState(initialState);
        registerAutomaton.setFinalStates(finalStates);
        registerAutomaton.setDataStates(dataStates.minimize(exceptions));
        registerAutomaton.setWordStates(wordStates.minimize(exceptions));
        registerAutomaton.setDataTransitions(dataTransitions.minimize(registerAutomaton.getWordStates(), registerAutomaton.getDataStates()));
        registerAutomaton.setWordTransitions(wordTransitions.minimize(registerAutomaton.getWordStates(), registerAutomaton.getDataStates()));
        //registerAutomaton.combineWordStates();
        return registerAutomaton;
    }

    public String toPythonScript() {
        String script = "import networkx as nx\n";
        script = script.concat("import matplotlib.pyplot as plt\n");
        script = script.concat("G = nx.DiGraph()\n");
        for (Transition transition : dataTransitions.getTransitions()) {
            DataTransition dataTransition = (DataTransition) transition;
            script = script.concat("G.add_edge('" + dataTransition.getSource().getLabel() + "', '" + dataTransition.getTarget().getLabel() + "', g='" + dataTransition.getTransitionLabel() + "')\n");
        }
        for (Transition transition : wordTransitions.getTransitions()) {
            WordTransition wordTransition = (WordTransition) transition;
            script = script.concat("G.add_edge('" + wordTransition.getSource().getLabel() + "', '" + wordTransition.getTarget().getLabel() + "', w='" + wordTransition.getWord() + "')\n");
        }
        script = script.concat("pos = nx.spring_layout(G)\n");
        script = script.concat("nx.draw_networkx_nodes(G, pos, nodelist=['" + initialState.getLabel() + "'], node_color='b')\n");
        script = script.concat("nx.draw_networkx_nodes(G, pos, nodelist=[");
        int i = 0;
        for (State state : dataStates.getStates()) {
            if (!state.equals(initialState)) {
                script = script.concat("'" + state.getLabel() + "'");
                script = script.concat(",");
            }
            i++;
        }
        i = 0;
        for (State state : wordStates.getStates()) {
            script = script.concat("'" + state.getLabel() + "'");
            if (i < wordStates.getStates().size() - 1) script = script.concat(",");
            i++;
        }
        script = script.concat("], node_color='r')\n");
        script = script.concat("nx.draw_networkx_edges(G, pos)\n");
        script = script.concat("nx.draw_networkx_edge_labels(G, pos)\n");
        script = script.concat("nx.draw_networkx_labels(G, pos)\n");
        script = script.concat("plt.show()");
        return script;
    }

    private void combineWordStates() {
        int size = wordStates.getStates().size();
        Map<WordState, WordState> replacementMap = new HashMap<>();
        for (State s1 : wordStates.getStates()) {
            for (State s2 : wordStates.getStates()) {
                WordState w1 = (WordState) s1;
                WordState w2 = (WordState) s2;
                boolean areEqual = !(w1 == w2);
                for (DataTransition d1 : s1.getDataTransitions()) {
                    boolean containsTransition = false;
                    for (DataTransition d2 : s2.getDataTransitions()) {
                        if (d1.isEquivalent(d2)) {
                            containsTransition = true;
                            break;
                        }
                    }
                    areEqual = areEqual && containsTransition;
                }
                if (areEqual && !(replacementMap.containsKey(w2) && replacementMap.get(w2).equals(w1))) replacementMap.put(w1, w2);
            }
        }
        for (Map.Entry<WordState, WordState> entry : replacementMap.entrySet()) {
            WordState keep = entry.getKey();
            WordState remove = entry.getValue();
            for (WordTransition w1 : remove.getWordTransitions()) {
                if (!keep.hasEquivalentWordTransition(w1)) {
                    WordTransition newWordTransition = new WordTransition(keep, (DataState) w1.getTarget(), w1.getWord());
                    keep.addWordTransition(newWordTransition);
                    wordTransitions.addTransition(newWordTransition);
                }
                wordTransitions.removeTransition(w1);
            }
            for (DataTransition d1 : remove.getDataTransitions()) {
                d1.getSource().getDataTransitions().remove(d1);
                dataTransitions.removeTransition(d1);
            }
            wordStates.removeState(remove);
        }
        if (size > wordStates.getStates().size()) {
            combineWordStates();
        }
    }

    public Pair<Graph, Map<State, Node>> toGraph() {
        Graph result = new Graph();
        Map<State, Node> stateNodeMap = new HashMap<>();

        statesToNodes(dataStates, result, stateNodeMap);
        statesToNodes(wordStates, result, stateNodeMap);

        transitionsToEdges(dataTransitions, result, stateNodeMap);
        transitionsToEdges(wordTransitions, result, stateNodeMap);

        return new Pair(result, stateNodeMap);
    }

    public boolean hasFinalState(State state) {
        return finalStates.contains(state);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterAutomaton that = (RegisterAutomaton) o;
        return dataStates.equals(that.dataStates) &&
                wordStates.equals(that.wordStates) &&
                initialState.equals(that.initialState) &&
                finalStates.equals(that.finalStates) &&
                wordTransitions.equals(that.wordTransitions) &&
                dataTransitions.equals(that.dataTransitions) &&
                register.equals(that.register);
    }

    private void transitionsToEdges(TransitionSet transitions, Graph result, Map<State, Node> stateNodeMap) {
        for (Transition transition : transitions.getTransitions()) {
            result.addEdge(new Edge(stateNodeMap.get(transition.getSource()), stateNodeMap.get(transition.getTarget()),
                    REMLexer.VOCABULARY.getLiteralName(REMParser.NULL)));
        }
    }

    private void statesToNodes(StateSet states, Graph graph, Map<State, Node> stateNodeMap) {
        for (State state : states.getStates()) {
            Node node = new Node(state.getLabel(), 0);
            graph.addNode(node);
            stateNodeMap.put(state, node);
        }
    }

    private void initialise() {
        this.dataStates = new StateSet();
        this.wordStates = new StateSet();
        this.finalStates = new StateSet();
        this.wordTransitions = new TransitionSet();
        this.dataTransitions = new TransitionSet();
    }
}

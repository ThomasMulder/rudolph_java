package util;

import REM.REMLexer;
import REM.REMParser;
import automaton.RegisterAutomaton;
import automaton.construction.RegisterAutomatonConstructionTree;
import automaton.construction.StateNameGenerator;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import java.util.HashSet;
import java.util.Set;

public class RegisterAutomatonBuilder {

    public RegisterAutomatonBuilder() {}

    public RegisterAutomaton build(String query) {
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(query);
        REMLexer conditionsLexer = new REMLexer(antlrInputStream);
        CommonTokenStream commonTokenStream = new CommonTokenStream(conditionsLexer);
        REMParser conditionsParser = new REMParser(commonTokenStream);
        conditionsParser.setBuildParseTree(true);
        RuleContext tree = conditionsParser.expression();
        RegisterAutomatonConstructionTree registerAutomatonConstructionTree = new RegisterAutomatonConstructionTree(null, tree);
        registerAutomatonConstructionTree.construct(countRegisterUsage(query));
        RegisterAutomaton registerAutomaton = registerAutomatonConstructionTree.getRegisterAutomaton();
        StateNameGenerator.getInstance().reset();
        return registerAutomaton.minimize();
    }

    public int countRegisterUsage(String query) {
        String[] queryParts = query.split(" ");
        Set<Integer> registerUsages = new HashSet<>();
        for (String part : queryParts) {
            if (part.contains("register")) {
                String[] subParts = part.split("register");
                for (String subPart : subParts) {
                    if (subPart.contains("(") && subPart.contains(")")) {
                        String[] registers = subPart.substring(1, subPart.length() - 1).split(",");
                        for (String register : registers) {
                            registerUsages.add(Integer.valueOf(register));
                        }
                    }
                }
            }
        }
        return registerUsages.size();
    }
}

package util;

import automaton.register.Register;
import automaton.state.State;
import graph.Node;

import java.util.LinkedList;

public class Run {
    private LinkedList<Node> path;
    private LinkedList<State> execution;
    private LinkedList<Register> registers;
    private int index, cost;

    public Run(Node node, State state, Register register, int index, int cost) {
        this.path = new LinkedList<>();
        this.path.add(node);
        this.execution = new LinkedList<>();
        this.execution.add(state);
        this.registers = new LinkedList<>();
        this.registers.add(register);
        this.index = index;
        this.cost = cost;
    }

    public Run(LinkedList<Node> path, LinkedList<State> execution, LinkedList<Register> registers, int index, int cost) {
        this.path = path;
        this.execution = execution;
        this.registers = registers;
        this.index = index;
        this.cost = cost;
    }

    public Node getNode() {
        return path.getLast();
    }

    public State getState() {
        return execution.getLast();
    }

    public Register getRegister() {
        return registers.getLast();
    }

    public int getCost() {
        return cost;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Run extend(Node node, State state, Register register, int index, int cost) {
        LinkedList<Node> extendedPath = new LinkedList<>();
        extendedPath.addAll(path);
        extendedPath.add(node);
        LinkedList<State> extendedExecution = new LinkedList<>();
        extendedExecution.addAll(execution);
        extendedExecution.add(state);
        LinkedList<Register> extendedRegisters = new LinkedList<>();
        extendedRegisters.addAll(registers);
        extendedRegisters.add(register);
        return new Run(extendedPath, extendedExecution, extendedRegisters, index, cost);
    }

    public boolean pathContainsNode(Node node) {
        for (Node n : path) {
            if (node.equals(n)) return true;
        }
        return false;
    }

    public boolean containsEqualEntry(Node node, State state, Register register) {
        for (int i = path.size() - 1; i >= 0; i--) {
            Node n = path.get(i);
            State s = execution.get(i);
            Register r = registers.get(i);
            if (n.equals(node)) {
                boolean result = s.equals(state) && r.equals(register);
                return result;
            }
        }
        return false;
    }

    public String pathToString() {
        if (path == null) return null;
        String result = "";
        for (Node node : path) {
            result = result.concat(node.getLabel() + "/");
        }
        return result.substring(0, result.length() - 1);
    }
}
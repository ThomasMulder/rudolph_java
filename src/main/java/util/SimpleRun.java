package util;

import automaton.register.Register;
import automaton.state.State;
import graph.Node;

public class SimpleRun {
    private Node node;
    private State state;
    private Register register;
    private int index;

    public SimpleRun(Node node, State state, Register register, int index) {
        this.node = node;
        this.state = state;
        this.register = register;
        this.index = index;
    }

    public Node getNode() {
        return node;
    }

    public State getState() {
        return state;
    }

    public Register getRegister() {
        return register;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}

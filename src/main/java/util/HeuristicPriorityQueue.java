package util;

import automaton.state.State;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HeuristicPriorityQueue {
    private final Map<State, Integer> heuristic;
    private List<Run> queue;
    private List<Integer> costs;

    public HeuristicPriorityQueue(Map<State, Integer> heuristic) {
        this.heuristic = heuristic;
        this.queue = new ArrayList();
        this.costs = new ArrayList<>();
    }

    public Run poll() {
        Run run = queue.get(0);
        queue.remove(0);
        costs.remove(0);
        return run;
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public int size() { return queue.size(); }

    public void insert(Run run) {
        int c = heuristic.get(run.getState()) + run.getCost();
        int i = getIndex(c);
        queue.add(i, run);
        costs.add(i, c);
    }

    private int getIndex(int c) {
        if (costs.isEmpty()) return 0;
        if (c < costs.get(0)) return 0;
        for (int i = 0; i < costs.size() - 1; i++) {
            int r = costs.get(i);
            int s = costs.get(i + 1);
            if (r <= c && c < s) return i + 1;
        }
        return costs.size();
    }

//    private int binarySearchIndex(int i, int c) {
//        int n = queue.size();
//        if (queue.isEmpty()) {
//            return 0;
//        }
//        if ((i == 0 && c < queue.get(i).getCost())) {
//            return 0;
//        }
//        if ((i == 0 && c == queue.get(i).getCost())) {
//            return 1;
//        }
//        if ((i == n - 1 && c >= queue.get(i).getCost())) {
//            return n;
//        }
//        if ((i == n - 1 && c < queue.get(i).getCost())) {
//            return i;
//        }
//        if (i > 0 && i < n - 1 && queue.get(i - 1).getCost() <= c && c <= queue.get(i + 1).getCost()) {
//            return i;
//        }
//        if (queue.get(i).getCost() > c) {
//            return binarySearchIndex((int) Math.floor(i / 2.0), c);
//        }
//        return binarySearchIndex(i + (int) Math.floor((n - i) / 2.0), c);
//    }
}

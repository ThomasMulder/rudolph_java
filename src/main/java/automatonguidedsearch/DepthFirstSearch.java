package automatonguidedsearch;

import automaton.RegisterAutomaton;
import automaton.register.Register;
import automaton.state.State;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.WordTransition;
import graph.Edge;
import graph.Graph;
import graph.Node;
import util.SimpleRun;

import java.util.*;

public class DepthFirstSearch extends AutomatonGuidedSearch {
    private int totalStackSize, maxStackSize;

    @Override
    public String perform(Graph graph, List<Node> sources, List<Node> targets, RegisterAutomaton registerAutomaton, List<String> statistics) {
        totalStackSize = 0;
        maxStackSize = 0;

        for (Node source : sources) {
            boolean arbitraryTarget = targets.size() == 1 && targets.get(0).equals(Graph.ARBITRARY_NODE);
            Stack<SimpleRun> stack = new Stack<>();
            stack.push(new SimpleRun(source, registerAutomaton.getInitialState(), registerAutomaton.getRegister(), 0));
            while (!stack.isEmpty()) {
                SimpleRun run = stack.peek();
                if (run.getIndex() < run.getNode().getOutgoingEdges().size()) {
                    Edge edge = run.getNode().getOutgoingEdges().get(run.getIndex());
                    run.setIndex(run.getIndex() + 1);
                    boolean checkRuns = stackContainsNode(stack, edge.getTarget());
                    Set<SimpleRun> runs = automatonStep(run, edge);
                    for (SimpleRun r : runs) {
                        if (arbitraryTarget) {
                            if (!r.getNode().equals(source)) {
                                boolean finalSteps = finalAutomatonSteps(r.getState(), registerAutomaton.getFinalStates(), r.getRegister(), edge.getSource().getValue(), edge.getTarget().getValue());
                                if (finalSteps) {
                                    updateStatistics(statistics, maxStackSize, totalStackSize);
                                    return stackToString(stack, edge.getTarget().getLabel());
                                }
                            }
                        } else if (targets.contains(edge.getTarget()) && !r.getNode().equals(source)) {
                            boolean finalSteps = finalAutomatonSteps(r.getState(), registerAutomaton.getFinalStates(), r.getRegister(), edge.getSource().getValue(), edge.getTarget().getValue());
                            if (finalSteps) {
                                updateStatistics(statistics, maxStackSize, totalStackSize);
                                return stackToString(stack, edge.getTarget().getLabel());
                            }
                        }
                        if (!checkRuns) {
                            if (stack.size() > maxStackSize) maxStackSize = stack.size();
                            totalStackSize++;
                            stack.push(r);
                        }
                        if (checkRuns && !stackContainsEqualEntry(stack, r.getNode(), r.getState(), r.getRegister())) {
                            r.setIndex(0);
                            if (stack.size() > maxStackSize) maxStackSize = stack.size();
                            totalStackSize++;
                            stack.push(r);
                        }
                    }
                } else {
                    stack.pop();
                }
            }
        }
        updateStatistics(statistics, maxStackSize, totalStackSize);
        return null;
    }

    private boolean stackContainsEqualEntry(Stack<SimpleRun> stack, Node node, State state, Register register) {
        List<SimpleRun> runs = stack.subList(0, stack.size());
        for (int i = runs.size() - 1; i >= 0; i--) {
            SimpleRun run = runs.get(i);
            Node n = run.getNode();
            State s = run.getState();
            Register r = run.getRegister();
            if (n.equals(node)) {
                boolean result = s.equals(state) && r.equals(register);
                return result;
            }
        }
        return false;
    }

    private String stackToString(Stack<SimpleRun> stack, String targetLabel) {
        if (stack.isEmpty()) return null;
        List<SimpleRun> runs = stack.subList(0, stack.size());
        String result = "";
        for (SimpleRun run : runs) {
            result = result.concat(run.getNode().getLabel() + "/");
        }
        return result.concat(targetLabel);
    }

    private boolean stackContainsNode(Stack<SimpleRun> stack, Node target) {
        List<SimpleRun> runs = stack.subList(0, stack.size());
        for (SimpleRun run : runs) {
            if (run.getNode().equals(target)) return true;
        }
        return false;
    }

    protected Set<SimpleRun> automatonStep(SimpleRun run, Edge edge) {
        State sourceState = run.getState();
        Register register = run.getRegister();
        Node sourceNode = edge.getSource();
        Node targetNode = edge.getTarget();
        String word = edge.getWord();
        Set<SimpleRun> result = new HashSet<>();
        for (DataTransition dataTransition : sourceState.getEnabledDataTransitions(word, sourceNode.getValue(), register)) {
            register = register.apply(dataTransition.getRegisterAssignment(), sourceNode.getValue());
            WordState target = (WordState) dataTransition.getTarget();
            for (WordTransition wordTransition : target.getEnabledWordTransitions(word, targetNode.getValue(), register)) {
                if ((wordTransition.getWord() == null || wordTransition.getWord().equals(NULL))) {
                    wordTransition.lockReverseTransitions();
                }
                result.add(new SimpleRun(targetNode, wordTransition.getTarget(), register, 0));
            }
        }
        return result;
    }

    @Override
    public String getName() {
        return "Depth-First Search";
    }

    @Override
    public String getShortName() {
        return "dfs";
    }
}

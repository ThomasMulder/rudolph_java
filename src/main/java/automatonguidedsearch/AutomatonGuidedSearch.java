package automatonguidedsearch;

import REM.REMLexer;
import REM.REMParser;
import automaton.RegisterAutomaton;
import automaton.register.Register;
import automaton.state.State;
import automaton.state.StateSet;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.WordTransition;
import graph.Edge;
import graph.Graph;
import graph.Node;
import util.Run;

import java.util.*;

public abstract class AutomatonGuidedSearch {
    protected static final String NULL = REMLexer.VOCABULARY.getLiteralName(REMParser.NULL);

    public AutomatonGuidedSearch() {}

    public abstract String perform(Graph graph, List<Node> sources, List<Node> targets, RegisterAutomaton registerAutomaton, List<String> statistics);

    public abstract String getName();

    public abstract String getShortName();

    protected Set<Run> automatonStep(Run run, Edge edge) {
        State sourceState = run.getState();
        Register register = run.getRegister();
        int index = run.getIndex();
        Node sourceNode = edge.getSource();
        Node targetNode = edge.getTarget();
        String word = edge.getWord();
        Set<Run> result = new HashSet<>();
        for (DataTransition dataTransition : sourceState.getEnabledDataTransitions(word, sourceNode.getValue(), register)) {
            register = register.apply(dataTransition.getRegisterAssignment(), sourceNode.getValue());
            WordState target = (WordState) dataTransition.getTarget();
            for (WordTransition wordTransition : target.getEnabledWordTransitions(word, targetNode.getValue(), register)) {
                if ((wordTransition.getWord() == null || wordTransition.getWord().equals(NULL))) {
                    wordTransition.lockReverseTransitions();
                    result.add(run.extend(sourceNode, wordTransition.getTarget(), register, 0, 0));
                } else {
                    result.add(run.extend(targetNode, wordTransition.getTarget(), register, 0, 0));
                }
            }
        }
        return result;
    }

    protected boolean finalAutomatonSteps(State sourceState, StateSet finalStates, Register register, int sourceValue, int targetValue) {
        if (sourceState instanceof WordState) throw new IllegalArgumentException("Cannot find final transition from word state.");
        for (DataTransition dataTransition : sourceState.getEnabledDataTransitions(NULL, targetValue, register)) {
            register = register.apply(dataTransition.getRegisterAssignment(), sourceValue);
            WordState target = (WordState) dataTransition.getTarget();
            if (finalStates.contains(target)) return true;
            for (WordTransition wordTransition : target.getEnabledWordTransitions(NULL, targetValue, register)) {
                return finalAutomatonSteps(wordTransition.getTarget(), finalStates, register, sourceValue, targetValue);
            }
        }
        return false;
    }

    protected void updateStatistics(List<String> statistics, int maxQueueSize, int totalQueueSize) {
        if (statistics != null) {
            statistics.clear();
            statistics.add(String.valueOf(maxQueueSize));
            statistics.add(String.valueOf(totalQueueSize));
        }
    }
}

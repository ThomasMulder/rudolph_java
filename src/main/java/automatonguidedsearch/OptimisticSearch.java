package automatonguidedsearch;

import automaton.RegisterAutomaton;
import automaton.register.Register;
import automaton.state.State;
import automaton.state.StateSet;
import automaton.state.WordState;
import automaton.transition.DataTransition;
import automaton.transition.WordTransition;
import graph.Edge;
import graph.Graph;
import graph.Node;
import util.HeuristicPriorityQueue;
import util.Pair;
import util.Run;

import java.util.*;

public class OptimisticSearch extends AutomatonGuidedSearch {
    private Map<State, Integer> heuristic;

    @Override
    public String perform(Graph graph, List<Node> sources, List<Node> targets, RegisterAutomaton registerAutomaton, List<String> statistics) {
        heuristic = calculateHeuristic(registerAutomaton);

        int totalQueueSize = 1;
        int maxQueueSize = 1;

        for (Node source : sources) {
            boolean arbitraryTarget = targets.size() == 1 && targets.get(0).equals(Graph.ARBITRARY_NODE);
            HeuristicPriorityQueue heuristicPriorityQueue = new HeuristicPriorityQueue(heuristic);
            heuristicPriorityQueue.insert(new Run(source, registerAutomaton.getInitialState(), registerAutomaton.getRegister(), 0, heuristic.get(registerAutomaton.getInitialState())));
            while (!heuristicPriorityQueue.isEmpty()) {
                Run run = heuristicPriorityQueue.poll();
                if (run.getNode() == null) throw new IllegalStateException("Encountered null element in queue.");
                for (Edge edge : run.getNode().getOutgoingEdges()) {
                    boolean checkRuns = run.pathContainsNode(edge.getTarget());
                    Set<Run> runs = automatonStep(run, edge);
                    for (Run r : runs) {
                        if (arbitraryTarget) {
                            if (!r.getNode().equals(source)) {
                                boolean finalSteps = finalAutomatonSteps(r.getState(), registerAutomaton.getFinalStates(), r.getRegister(), edge.getSource().getValue(), edge.getTarget().getValue());
                                if (finalSteps) {
                                    updateStatistics(statistics, maxQueueSize, totalQueueSize);
                                    return r.pathToString();
                                }
                            }
                        } else if (targets.contains(edge.getTarget()) && !r.getNode().equals(source)) {
                            boolean finalSteps = finalAutomatonSteps(r.getState(), registerAutomaton.getFinalStates(), r.getRegister(), edge.getSource().getValue(), edge.getTarget().getValue());
                            if (finalSteps) {
                                updateStatistics(statistics, maxQueueSize, totalQueueSize);
                                return r.pathToString();
                            }
                        }
                        if (!checkRuns) {
                            totalQueueSize++;
                            heuristicPriorityQueue.insert(r);
                            if (heuristicPriorityQueue.size() > maxQueueSize)
                                maxQueueSize = heuristicPriorityQueue.size();
                        }
                        if (checkRuns && !run.containsEqualEntry(r.getNode(), r.getState(), r.getRegister())) {
                            totalQueueSize++;
                            heuristicPriorityQueue.insert(r);
                            if (heuristicPriorityQueue.size() > maxQueueSize)
                                maxQueueSize = heuristicPriorityQueue.size();
                        }
                    }
                }
            }
        }
        updateStatistics(statistics, maxQueueSize, totalQueueSize);
        return null;
    }

    @Override
    public String getName() {
        return "Optimistic Search";
    }

    @Override
    public String getShortName() {
        return "os";
    }

    public Map<State, Integer> calculateHeuristic(RegisterAutomaton registerAutomaton) {
        Map<State, Integer> result = initialiseHeuristic(registerAutomaton);
        Pair<Graph, Map<State, Node>> p = registerAutomaton.toGraph();
        Graph graph = p.getKey();
        Map<State, Node> stateNodeMap = p.getValue();

        for (State state : registerAutomaton.getFinalStates().getStates()) {
            Node source = stateNodeMap.get(state);
            Map<Node, Integer> shortestPaths = computeShortestPaths(graph, source);
            updateShortestPaths(result, shortestPaths, stateNodeMap);
        }

        return result;
    }

    @Override
    protected Set<Run> automatonStep(Run run, Edge edge) {
        Set<Run> result = new HashSet<>();
        State sourceState = run.getState();
        Register register = run.getRegister();
        int index = run.getIndex();
        Node sourceNode = edge.getSource();
        Node targetNode = edge.getTarget();
        String word = edge.getWord();
        for (DataTransition dataTransition : sourceState.getEnabledDataTransitions(word, sourceNode.getValue(), register)) {
            register = register.apply(dataTransition.getRegisterAssignment(), sourceNode.getValue());
            WordState target = (WordState) dataTransition.getTarget();
            for (WordTransition wordTransition : target.getEnabledWordTransitions(word, targetNode.getValue(), register)) {
                if ((wordTransition.getWord() == null || wordTransition.getWord().equals(NULL))) {
                    wordTransition.lockReverseTransitions();
                }
                result.add(run.extend(targetNode, wordTransition.getTarget(), register, index, heuristic.get(wordTransition.getTarget()) + 2));
            }
        }
        return result;
    }

    private void updateShortestPaths(Map<State, Integer> result, Map<Node, Integer> shortestPaths, Map<State, Node> stateNodeMap) {
        if (result.size() != shortestPaths.size()) throw new IllegalStateException("Shortest path maps cannot be of different size.");

        for (Map.Entry<Node, Integer> shortestPath : shortestPaths.entrySet()) {
            State state = getStateByNode(stateNodeMap, shortestPath.getKey());
            if (result.get(state) > shortestPath.getValue()) {
                result.put(state, shortestPath.getValue());
            }
        }
    }

    private State getStateByNode(Map<State, Node> stateNodeMap, Node key) {
        for (Map.Entry<State, Node> stateNodeEntry : stateNodeMap.entrySet()) {
            if (stateNodeEntry.getValue().equals(key)) return stateNodeEntry.getKey();
        }
        throw new IllegalStateException("No corresponding state found for node " + key.getLabel());
    }

    private Map<Node,Integer> computeShortestPaths(Graph graph, Node source) {
        Map<Node, Integer> result = initialiseGraph(graph, source);

        for (Node node : graph.getNodes()) {
            for (Edge edge : graph.getEdges()) {
                if (result.get(edge.getTarget()) + 1 < result.get(edge.getSource())) {
                    result.put(edge.getSource(), result.get(edge.getTarget()) + 1);
                }
            }
        }

        return result;
    }

    private Map<Node,Integer> initialiseGraph(Graph graph, Node source) {
        Map<Node, Integer> result = new HashMap<>();
        for (Node node : graph.getNodes()) {
            result.put(node, Integer.MAX_VALUE - 1);
        }
        result.put(source, 0);
        return result;
    }

    private Map<State, Integer> initialiseHeuristic(RegisterAutomaton registerAutomaton) {
        Map<State, Integer> result = new HashMap<>();
        initialiseStateSet(registerAutomaton.getDataStates(), result);
        initialiseStateSet(registerAutomaton.getWordStates(), result);
        return result;
    }

    private void initialiseStateSet(StateSet states, Map<State, Integer> result) {
        for (State state : states.getStates()) {
            result.put(state, Integer.MAX_VALUE - 1);
        }
    }
}

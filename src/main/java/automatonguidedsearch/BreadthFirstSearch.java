package automatonguidedsearch;

import automaton.RegisterAutomaton;
import graph.Edge;
import graph.Graph;
import graph.Node;
import util.Run;

import java.util.*;

public class BreadthFirstSearch extends AutomatonGuidedSearch {

    public BreadthFirstSearch() {}

    @Override
    public String perform(Graph graph, List<Node> sources, List<Node> targets, RegisterAutomaton registerAutomaton, List<String> statistics) {
        int totalQueueSize = 1;
        int maxQueueSize = 1;

        for (Node source : sources) {
            boolean arbitraryTarget = targets.size() == 1 && targets.get(0).equals(Graph.ARBITRARY_NODE);
            Queue<Run> queue = new LinkedList<>();
            queue.add(new Run(source, registerAutomaton.getInitialState(), registerAutomaton.getRegister(), 0, 0));
            while (!queue.isEmpty()) {
                Run run = queue.poll();
                Node node = run.getNode();
                if (node == null) throw new IllegalStateException("Encountered null element in queue.");
                for (Edge edge : node.getOutgoingEdges()) {
                    boolean checkRuns = run.pathContainsNode(edge.getTarget());
                    Set<Run> runs = automatonStep(run, edge);
                    for (Run r : runs) {
                        if (arbitraryTarget) {
                            if (!r.getNode().equals(source)) {
                                boolean finalSteps = finalAutomatonSteps(r.getState(), registerAutomaton.getFinalStates(), r.getRegister(), edge.getSource().getValue(), edge.getTarget().getValue());
                                if (finalSteps) {
                                    updateStatistics(statistics, maxQueueSize, totalQueueSize);
                                    return r.pathToString();
                                }
                            }
                        } else if (targets.contains(edge.getTarget()) && !r.getNode().equals(source)) {
                            boolean finalSteps = finalAutomatonSteps(r.getState(), registerAutomaton.getFinalStates(), r.getRegister(), edge.getSource().getValue(), edge.getTarget().getValue());
                            if (finalSteps) {
                                updateStatistics(statistics, maxQueueSize, totalQueueSize);
                                return r.pathToString();
                            }
                        }
                        if (!checkRuns) {
                            totalQueueSize++;
                            queue.add(r);
                            if (queue.size() > maxQueueSize) maxQueueSize = queue.size();
                        }
                        if (checkRuns && !run.containsEqualEntry(r.getNode(), r.getState(), r.getRegister())) {
                            totalQueueSize++;
                            queue.add(r);
                            if (queue.size() > maxQueueSize) maxQueueSize = queue.size();
                        }
                    }
                }
            }
        }
        updateStatistics(statistics, maxQueueSize, totalQueueSize);
        return null;
    }

    @Override
    public String getName() {
        return "Breadth-First Search";
    }

    @Override
    public String getShortName() {
        return "bfs";
    }
}

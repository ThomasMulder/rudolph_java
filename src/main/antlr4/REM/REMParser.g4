parser grammar REMParser;

options { tokenVocab = REMLexer; }

expression
    : LPARENTHESIS expression RPARENTHESIS
    | expression OR expression
    | expression BEFORE expression
    | REPEAT LPARENTHESIS expression RPARENTHESIS
    | expression IF LPARENTHESIS condition RPARENTHESIS
    | REGISTER LPARENTHESIS assignment RPARENTHESIS expression
    | LETTER
    | NULL;

assignment
    : NUMBER COMMA assignment
    | NUMBER;

condition
    : LPARENTHESIS condition RPARENTHESIS
    | condition CAND condition
    | condition COR condition
    | CNOT condition
    | REGEQ LPARENTHESIS NUMBER RPARENTHESIS
    | REGNEQ LPARENTHESIS NUMBER RPARENTHESIS
    | EQ LPARENTHESIS NUMBER RPARENTHESIS
    | NEQ LPARENTHESIS NUMBER RPARENTHESIS;
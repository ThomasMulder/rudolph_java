lexer grammar REMLexer;

// Expression level symbols
LPARENTHESIS    :   '(';
RPARENTHESIS    :   ')';
OR  :   'or';
BEFORE :   'before';
REPEAT :   'repeat';
IF  :   'if';
REGISTER    : 'register';
COMMA   :   ',';
NULL    :   'null';


// Condition level symbols
CAND     :   '&&';
COR      :   '||';
CNOT     :   '!';
REGEQ   :   'regeq';
REGNEQ  :   'regneq';
EQ  :   'eq';
NEQ :   'neq';

WHITESPACE  :   (' '|'\t'|'\r'? '\n')+ -> channel(HIDDEN);

LETTER  :   [a-z];
NUMBER  :   DIGIT+;

fragment
DIGIT   :   [0-9];